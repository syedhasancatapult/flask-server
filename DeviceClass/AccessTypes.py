from enum import Enum

class AccessTypes(Enum):
    DOOR_OPEN = 'door open'
    CARD_SWIPE = 'card swipe'
    DOOR_OPEN_0 = 'door open 0 swipes'
    DOOR_OPEN_1 = 'door open 1 swipe'
    DOOR_OPEN_2 = 'door open 2 swipes'
    DOOR_CLOSE_0 = 'door close 0 swipes'
    DOOR_CLOSE_1 = 'door close 1 swipe'
    DOOR_CLOSE_2 = 'door close 2 swipes'
    CONTAINER_SAFE_ACCESS = 'container safe access'
    CONTAINER_ALARM = 'container alarm'
