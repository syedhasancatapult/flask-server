from Database.Postgres import Postgres
from datetime import datetime
from DeviceClass.AlarmTypes import AlarmType
from .ConditionMonitoring import ConditionMonitoring
import logging
import os
from dotenv import load_dotenv

def raise_alarm(
        alarm_type: AlarmType,
        container_id: int,
        timestamp: datetime = datetime.now(),
        description: str = "An alarm has been raised",
        item_id: int = None,
        user_id: int = None
):
    """
    Raise an alarm
    :param alarm_type: An alarm type from the enum class:
    :param timestamp: Defaults to now, must be of type datetime.datetime
    :param description: A description about the alarm
    :param container_id: The container the alarm is regarding
    :param item_id: The item this is regarding
    :param user_id: The user this is regarding
    """
    db = Postgres()
    sql = """
    INSERT INTO alarms (timestamp, description, item_id, user_id, container_id, alarm_type_id, alarm_active_notactive)
    VALUES ('{}', '{}', {}, {}, {}, {}, true)
    """.format(
        timestamp,
        description,
        'null' if item_id is None else item_id,
        'null' if user_id is None else user_id,
        container_id,
        alarm_type
    )
    db.put(sql)


def get_container_id() -> int:
    dotenv_path = '/home/das/flask-server/.env'
    load_dotenv(dotenv_path)
    return int(os.environ.get("CONTAINER_ID"))


def raise_alarm_cloud(
        alarm_type: int,
        container_id: int,
        timestamp: datetime = datetime.now(),
        description: str = "An alarm has been raised"
):
    """
    Raise an alarm
    :param alarm_type: An alarm type must be from one of the defined alarm types:
     "Incompatibility", "Unauthorised access", "Stock levels", "Threshold breached", "Unexpected item"
    :param timestamp: Defaults to now, must be of type datetime.datetime
    :param description: A description about the alarm
    :param container_id: The container the alarm is regarding
    """
    try:
        db = Postgres(
            host="dasdb.cnmplr74tcrx.eu-west-2.rds.amazonaws.com",
            port=5432,
            dbname="das",
            username="postgres",
            password="postgres"
        )
        sql = """
            INSERT INTO alarms (timestamp, description, container_id, alarm_type_id, alarm_active_notactive)
            VALUES ('{}', '{}', {}, {}, true)
            """.format(timestamp, description, container_id, alarm_type)
        db.put(sql)
    except Exception as e:
        print(e)


def get_incompatibilities_by_device_mac(mac_address):
    db = Postgres()
    sql = """
    SELECT type_id, incompatibility FROM item_type WHERE type_id = (
        SELECT type_id FROM items where item_id = (
            SELECT item_id FROM device_item
            WHERE device_id = (
                SELECT device_id FROM devices where mac_address = '{}'
            )
        )
    )
    """.format(mac_address)
    result = db.get(sql)
    # Returns a list of incompatability item types
    id, incompatability =result[0]
    mystring = incompatability.split(',')
    arr = []
    for each in mystring:
        try:
            arr.append(int(each))
        except Exception as e:
            error = e

    return arr


def string_to_array(string: str) -> list:
    """
    Separates strings by commas and returns an array
    :param string: comma separated string
    :return: array of strings
    """
    my_string = string.split(',')
    arr = []
    for each in my_string:
        try:
            arr.append(int(each))
        except Exception as e:
            error = e

    return arr


def raise_item_threshold_alarm(alarm_string, ts, container_id: int):

    cloud = Postgres()

    sql = """INSERT INTO alarms (timestamp, description, container_id, alarm_type_id, alarm_active_notactive)
    VALUES ('{}', '{}', {}, {}, true)
    """.format(
        ts,
        alarm_string,
        container_id,
        3
    )
    cloud.put(sql)
    try:
        db = Postgres(
            host="dasdb.cnmplr74tcrx.eu-west-2.rds.amazonaws.com",
            port = 5432,
            dbname = "das",
            username = "postgres",
            password = "wIe8kaes"
            )
        #db.put(sql)
    except Exception as e:
        print(e)


def raise_item_stock_alarm(item, alarm_string, ts, container_id: int):
    cloud = Postgres()
    sql = """INSERT INTO alarms (timestamp, description, item_id, container_id, alarm_type_id, alarm_active_notactive)
    VALUES ('{}', '{}', {}, {}, {}, true)
    """.format(
        ts,
        alarm_string,
        item,
        container_id,
        5
    )
    cloud.put(sql)
    try:
        db = Postgres(
            host="dasdb.cnmplr74tcrx.eu-west-2.rds.amazonaws.com",
            port = 5432,
            dbname = "das",
            username = "postgres",
            password = "wIe8kaes"
            )
        #db.put(sql)
    except Exception as e:
        print(e)


def raise_item_incompatibility_alarm(item, alarm_string, ts, container_id: int):
    cloud = Postgres()
    print('raising alarm')
    # insert into alarms table
    sql = """INSERT INTO alarms (timestamp, description, item_id, container_id, alarm_type_id, alarm_active_notactive)
    VALUES ('{}', '{}', {}, {}, {}, true)
    """.format(
        ts,
        alarm_string,
        item,
        container_id,
        1
    )
    cloud.put(sql)
    try:
        db = Postgres(
            host="dasdb.cnmplr74tcrx.eu-west-2.rds.amazonaws.com",
            port=5432,
            dbname="das",
            username="postgres",
            password="postgres"
            )
        #db.put(sql)
    except Exception as e:
        print(e)

def get_incompatibility_alarm_by_ich(ich:int):
    sql = """SELECT * FROM alarms 
    WHERE description LIKE '%' || {} || '%'
    """.format(ich)
    db= Postgres()
    results = db.get(sql)
    return len(results)

def get_item_incompatibility_by_item_id(item_id):
    sql = """SELECT i.item_id, i.type_id, it.incompatibility, ich.item_history_id FROM item_container_history as ich
            INNER JOIN items AS i ON (ich.item_id=i.item_id)
            INNER JOIN item_type AS it ON (i.type_id=it.type_id)
            WHERE ich.checked_in_out
            AND i.item_id = {}
            AND NOT i.alarm
    """.format(item_id)
    cloud = Postgres()
    results = cloud.get(sql)

    item_id, type_id, incompatability_list, ich_id = results[0]
    obj = {
        'item_history_id': ich_id,
        'item_id': item_id,
        'type_id': type_id,
        'list': string_to_array(incompatability_list)
    }
    return obj


def get_item_type_by_id(type_id):
    db = Postgres()
    sql = """SELECT type_id, type FROM item_type
    WHERE type_id = {}
    """.format(type_id)
    type_id, item_type = db.get(sql)[0]
    return item_type


def get_item_by_id(item_id):
    db = Postgres()
    sql = """
    SELECT item_id, type_id, alarm, expiry_date
    FROM items
    WHERE item_id = {}
    """.format(item_id)
    item_id, type_id, alarm, expiry_date = db.get_one(sql)
    return item_id, type_id, alarm, expiry_date


def get_containers_current_item_incompatibility():
    db = Postgres()
    sql = """SELECT i.item_id, i.type_id, it.incompatibility, ich.item_history_id FROM item_container_history as ich
            INNER JOIN items AS i ON (ich.item_id=i.item_id)
            INNER JOIN item_type AS it ON (i.type_id=it.type_id)
            WHERE ich.checked_in_out
    """
    results = db.get(sql)
    list = []
    for result in results:
        item_id, type_id, string_list, ich_id = result
        obj = {
            'item_history_id': ich_id,
            'item_id': item_id,
            'type_id': type_id,
            'list': string_to_array(string_list)
        }
        list.append(obj)

    return list


def set_item_alarm_state(item: int, alarm: bool):
    sql = """UPDATE items
    SET alarm = {}
    WHERE item_id = {}""".format(alarm, item)
    db = Postgres()
    db.put(sql)


def set_item_type_alarm_state(item_type: int, alarm: bool):
    """
    Sets the alarm of items that belong to a given type
    :param item_type:
    :param alarm:
    :return:
    """
    sql = """UPDATE items
        SET alarm = {}
        WHERE type_id = {}""".format(alarm, item_type)
    db = Postgres()
    db.put(sql)


def humid_out_of_range_item_type(humid: int) -> list:
    """
    gets back item types out of their temperature range
    :param humid: current temperature in the container
    :return: list of item types that an alarm needs to be raised on
    """
    sql = """select distinct it.type_id, it.type, it.temp_min, it.temp_max, it.humid_min, it.humid_max
        from item_container_history h, items i, item_type it
        where h.item_id=i.item_id 
        and i.type_id = it.type_id 
        and h.checked_in_out='true'
        and i.alarm = 'false'
        and (humid_min > {} or humid_max < {})""".format(humid, humid)
    db = Postgres()
    result = db.get(sql)
    return result


def access_card_security_check(access_card_uid:str) -> int:
    db = Postgres()
    results = db.get("SELECT user_id, accesscard FROM users WHERE accesscard = '{}'".format(access_card_uid))
    if len(results) == 0:
        raise_alarm(
            AlarmType.UnauthorisedAccess,
            get_container_id(),
            datetime.now(),
            "The card tapped in {} does not belong to a registered user".format(access_card_uid)
        )
        return 0
    user_id, card = results[0]
    return int(user_id)


def temp_out_of_range_item_type(temp: int) -> list:
    """
    gets back item types out of their temperature range
    :param temp: current temperature in the container
    :return: list of item types that an alarm needs to be raised on
    """
    sql = """select distinct it.type_id, it.type, it.temp_min, it.temp_max, it.humid_min, it.humid_max
        from item_container_history h, items i, item_type it
        where h.item_id=i.item_id 
        and i.type_id = it.type_id 
        and h.checked_in_out='true'
        and i.alarm = 'false'
        and (temp_min > {} or temp_max < {})""".format(temp, temp)
    db = Postgres()
    result = db.get(sql)
    return result

def condition_monitoring_event_management(cm_object) -> str:
    try:
        cm = ConditionMonitoring(cm_object)
        logging.info(cm.set_device_type())
        event = cm.send()
        if event == "log_out":
            return event
        return "200"
    except Exception as e:
        print("EXCEPTION", flush=True)
        print(e, flush=True)
        return str(e)

def door_mimic(door_mac, state:bool):
    """
    :param door_mac:
    :param state: boolean, True for Open False for close
    :return:
    """
    door_cm_object = {
        "addr": door_mac,
        "timestamp": datetime.now().strftime("%m/%d/%Y, %H:%M:%S"),
        "parsed_data": {"door_state": "open" if state else "closed"}
    }

    condition_monitoring_event_management(door_cm_object)

def active_alarm_already_exist_by_description(description:str) -> int:
    sql = """SELECT alarm_id, description, alarm_active_notactive FROM alarms 
    WHERE alarm_active_notactive = true  
    AND description = '{}'
    ORDER BY timestamp DESC
    LIMIT 1 
    """.format(description)
    db = Postgres()
    result = db.get(sql)
    if len(result)>0:
        alarm_id, description, alarm_active_notactive = result[0]
        return alarm_id
    return 0

def update_alarm_timestamp(alarm_id):
    sql = """UPDATE alarms
            SET timestamp = '{}'
            WHERE alarm_id = {}""".format(datetime.now(), alarm_id)
    db = Postgres()
    db.put(sql)
