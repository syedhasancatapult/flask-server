from datetime import datetime
import json
from decimal import Decimal
import os
import psycopg2


def psql_get(sql):
    result = 0
    try:
        conn = psycopg2.connect(
            host="localhost",
            port=5432,
            dbname="das",
            user="postgres",
            password="password"
        )

        cur = conn.cursor()
        sql = sql + ';'
        cur.execute(sql)
        result = cur.fetchall()
    finally:
        try:
            conn.close()
        except:
            pass
    return result

def psql_put(sql):
    result = 0
    try:
        conn = psycopg2.connect(
            host="localhost",
            port=5432,
            dbname="das",
            user="postgres",
            password="password"
        )

        cur = conn.cursor()
        cur.execute(sql + ';')
        conn.commit()
    finally:
        try:
            cur.close()
            conn.close()
        except:
            pass


def lambda_handler(event, context):
    # TODO implement
    path = event['path']

    if path == '/ui/container-management':
        containers = []

        sql = """SELECT * FROM containers
        """

        results = psql_get(sql)
        arr = []
        for result in results:
            id, description, size, capacity = result

            obj = {
                "Id": id,
                "Description": description,
                "Size": size,
                "Capacity": capacity
            }

            arr.append(obj)
        return {
        'statusCode': 200,
        'headers':{'Access-Control-Allow-Origin':'*'},
        'body': json.dumps(arr)
        }
    if path == '/ui/container-management/add':
        body = json.loads(event['body'])
        if body['id'] == 0:
            # add new container
            sql = """INSERT INTO containers(description, size, capacity)
            VALUES ('{}', {}, {});
            """.format(body['cd'], body['cs'], body['cc'])
        else:
            # update container
            sql = """UPDATE containers
            SET description = '{}',
                size = {},
                capacity = {}
                WHERE container_id = {};
            """.format(body['cd'], body['cs'], body['cc'], body['id'])

        psql_put(sql)

        return {
        'statusCode': 200,
        'headers':{'Access-Control-Allow-Origin':'*'},
        'body': json.dumps("container updated")
        }

    if path == '/ui/containers':
        body = event['queryStringParameters']
        containers = []

        sql = """with j as (
            with t as (select distinct container_id, max(timestamp) as latest
            from container_history
            where gps_lat is not null
            group by container_id)
            select distinct c.container_id, t.latest, c.gps_lat, c.gps_long, c.altitude
            from t left join container_history c
            on c.container_id=t.container_id and t.latest=c.timestamp
            order by container_id)
            select distinct j.container_id, s.description, j.latest, j.gps_lat, j.gps_long, j.altitude
            from j inner join containers s
            on j.container_id = s.container_id
            order by j.container_id
        """
        if body:
            for container in json.loads(body['containers']):
                containers.append(container)
            sql = """select item_container_history.container_id,
                count (item_type.type_id) as number_of_items,
                item_type.type
                from item_container_history, items, item_type
                where (item_container_history.item_id=items.item_id and items.type_id=item_type.type_id and item_container_history.checked_in_out = 'true')
                group by item_container_history.container_id, item_type.type_id;
            """
            results = psql_get(sql)
            arr = []
            for result in results:
                container_id, count, item_type = result
                if container_id in containers:
                    obj = {
                        "Container ID": container_id,
                        "Count": count,
                        "Item Type": item_type
                    }
                    arr.append(obj)
            return {
                'statusCode': 200,
                'headers':{'Access-Control-Allow-Origin':'*'},
                'body': json.dumps(arr)
                }

        results = psql_get(sql)
        arr = []
        for result in results:
            id, description, ts, lat, lon, altitude = result

            obj = {
                "Id": id,
                "Description": description,
                "Timestamp": ts.strftime("%m/%d/%Y, %H:%M:%S"),
                "Lattitude": float(lat),
                "Longitude": float(lon),
                "Altitude": altitude
            }
            arr.append(obj)
        return {
        'statusCode': 200,
        'headers':{'Access-Control-Allow-Origin':'*'},
        'body': json.dumps(arr)
        }
    elif path == '/ui/condition-monitoring':
        body = event['queryStringParameters']
        if 't1' in body:
            latest = False
            container_id = body['container_id']
            start_ts = body['t1']
            end_ts = body['t2']
            sql = """SELECT * FROM container_history WHERE container_id = {}
            AND timestamp BETWEEN {} AND {}
            AND temperature IS NOT NULL
            AND humidity IS NOT NULL
            AND light != 'null'
            ORDER BY timestamp ASC
            """.format(container_id, start_ts, end_ts)
        else:
            latest = True
            container_id = body['container_id']
            sql = """SELECT * FROM container_history WHERE container_id = {}
            AND temperature IS NOT NULL
            AND humidity IS NOT NULL
            AND light != 'null'
            ORDER BY timestamp DESC
            LIMIT 1
            """.format(container_id)
        results = psql_get(sql)
        arr = []
        for result in results:
            id, timestamp, container_id, gps_lat, gps_long, temp, humid, shock, altitude, battery_level, light = result
            obj = {
                "Temperature (C)": float(temp),
                "Humidity (%)": float(humid),
                "Shock (%)": float(shock),
                "Light": light,
                "Timestamp": timestamp.strftime("%m/%d/%Y, %H:%M:%S"),
                "Latest": latest
            }
            arr.append(obj)
        return {
        'statusCode': 200,
        'headers':{'Access-Control-Allow-Origin':'*'},
        'body': json.dumps(arr)
        }

    elif path == '/ui/login':
        body = event['body']
        user = body['username']
        passw = body['password']
        sql = """SELECT * FROM users where username='{}'
        """.format(user)
        results = psql_get(sql)

        if len(results) > 0:
            user_id, name, username, password, accesscard, role_id, active = results[0]
            if passw == password:

                obj = {
                    "id": user_id,
                    "name": name,
                    "username": username,
                    "accesscard": accesscard,
                    "role_id": role_id
                }
                return {
                    'statusCode': 200,
                    'headers':{'Access-Control-Allow-Origin':'*'},
                    'body': json.dumps(obj)
                    }
            else:
                return {
                    'statusCode': 403,
                    'headers':{'Access-Control-Allow-Origin':'*'},
                    'body': json.dumps('password incorrect')
                    }

        else:
            return {
                    'statusCode': 404,
                    'headers':{'Access-Control-Allow-Origin':'*'},
                    'body': json.dumps('user not found')
                    }

    elif path == '/container/metrics':
        body = event['queryStringParameters']
        if 'id' in body:
            sql = 'SELECT * FROM container_history WHERE container_id = ' + body['id'] + ' ORDER BY timestamp DESC'
            results = psql_get(sql)
            arr = []
            for result in results:
                id, light, timestamp, container_id, gps_lat, gps_long, temp, humid, shock, altitude, battery_level = result
                obj = {
                    "Lattitude": float(gps_lat),
                    "Longitude": float(gps_long),
                    "Temperature (C)": float(temp),
                    "Humidity (%)": float(humid),
                    "Shock (%)": float(shock),
                    "Light (lux)": light,
                    "Timestamp": timestamp.strftime("%m/%d/%Y, %H:%M:%S")
                }
                arr.append(obj)
            return {
            'statusCode': 200,
            'headers':{'Access-Control-Allow-Origin':'*'},
            'body': json.dumps(arr)
            }
        else:
            return {
            'statusCode': 400,
            'headers':{'Access-Control-Allow-Origin':'*'},
            'body': json.dumps("you must provide an id for the container (/container/metrics?id=1)")
            }
    elif path == '/ui/container/alarms/update':
        body = event['queryStringParameters']
        sql = """ UPDATE alarms
                SET alarm_active_notactive = {}, timestamp_deactivation = '{}', user_id = {}
                WHERE alarm_id = {}
                """.format(body['setAlarm'], body['ts'], body['user'], body['alarm'])
        psql_put(sql)
        return {
                'statusCode': 200,
                'headers':{'Access-Control-Allow-Origin':'*'},
                'body': json.dumps("alarm updated")
                }
    elif path == '/ui/container/alarms/update/all':
        body = event['body']
        user = body['user']
        ts = body['ts']
        setAlarm = body['setAlarm']

        sql = """ UPDATE alarms
                SET alarm_active_notactive = {}, timestamp_deactivation = '{}', user_id = {}
                WHERE alarm_active_notactive = 'true'
                """.format(setAlarm, ts, user)
        psql_put(sql)
        return {
                'statusCode': 200,
                'headers':{'Access-Control-Allow-Origin':'*'},
                'body': json.dumps("alarm updated")
                }
    elif path == '/ui/container/alarms':
        body = event['queryStringParameters']
        if 'id' in body:
            container_id = body['id']
            if len(body) == 1:
                sql = """SELECT alarms.alarm_id, alarms.timestamp, alarms.description, alarm_type.type, alarms.alarm_active_notactive
                FROM alarms INNER JOIN alarm_type ON (alarms.alarm_type_id=alarm_type.alarm_type_id)
                WHERE alarms.container_id = {}
                ORDER BY alarms.timestamp DESC
                """.format(container_id)
                results = psql_get(sql)
                arr = []
                for result in results:
                    alarm_id, timestamp, description, alarm_type, alarm_status= result
                    obj = {
                        "Id": alarm_id,
                        "Alarm": alarm_type,
                        "Description": description,
                        "Timestamp": timestamp.strftime("%m/%d/%Y, %H:%M:%S"),
                        "Status": "Active" if alarm_status else "Inactive"
                    }
                    arr.append(obj)
                return {
                'statusCode': 200,
                'headers':{'Access-Control-Allow-Origin':'*'},
                'body': json.dumps(arr)
                }
            else:
                clauses = []
                if 'at' in body:
                    clauses.append(' and alarm_type.type IN {}'.format(body['at']))
                if 'as' in body:
                    clauses.append(' and alarms.alarm_active_notactive IN {}'.format(body['as']))
                if 't1' in body:
                    clauses.append(" and timestamp BETWEEN '{}' AND '{}'".format(body['t1'], body['t2']))

                sql = """SELECT alarms.alarm_id, alarms.timestamp, alarms.description, alarm_type.type, alarms.alarm_active_notactive
                    FROM alarms INNER JOIN alarm_type ON (alarms.alarm_type_id=alarm_type.alarm_type_id)
                    WHERE alarms.container_id = {}
                """.format(container_id)
                for clause in clauses:
                    sql = sql + clause
                sql = sql +  ' ORDER BY timestamp DESC'
                results = psql_get(sql)
                arr = []
                for result in results:
                    alarm_id, timestamp, description, alarm_type, alarm_status= result
                    obj = {
                        "Id": alarm_id,
                        "Alarm": alarm_type,
                        "Description": description,
                        "Timestamp": timestamp.strftime("%m/%d/%Y, %H:%M:%S"),
                        "Status": "Active" if alarm_status else "Inactive"
                    }
                    arr.append(obj)
                return {
                'statusCode': 200,
                'headers':{'Access-Control-Allow-Origin':'*'},
                'body': json.dumps(arr)
                }

        else:
            return {
            'statusCode': 400,
            'headers':{'Access-Control-Allow-Origin':'*'},
            'body': json.dumps("you must provide an id for the container (/ui/container/alarms?id=1)")
            }

    elif path == '/ui/container/items':
        body = event['queryStringParameters']
        if 'id' in body:
            sql = """SELECT cst.container_stock_th_id, 
                        tmptable.number_of_items,
                        COALESCE(it.type, tmptable.type) AS type_name,
                        cst.amber_th, 
                        cst.red_th
                    FROM (
                        SELECT it.type, it.type_id, COUNT (it.type_id) AS number_of_items, ich.container_id
                        FROM item_container_history AS ich 
                        JOIN items AS i 
                        ON i.item_id = ich.item_id 
                        JOIN item_type AS it
                        ON i.type_id = it.type_id
                        WHERE ich.checked_in_out = true
                        GROUP BY it.type_id, ich.container_id
                    ) AS tmptable 
                    FULL OUTER JOIN container_stock_thresholds AS cst
                    JOIN item_type AS it
                    ON cst.item_type_id = it.type_id
                    ON tmptable.type_id = cst.item_type_id
                    WHERE cst.container_id = {} OR tmptable.container_id = {}
                    ORDER BY UPPER(COALESCE(it.type, tmptable.type)) ASC
                    """.format(body['id'],body['id'])
            results = psql_get(sql)
            arr = []
            for result in results:
                container_stock_th_id, count, item_type, amber, red = result
                obj = {
                    "Id": container_stock_th_id,
                    "Item Type": item_type,
                    "Count": count,
                    "Amber threshold": amber,
                    "Red threshold": red
                }
                arr.append(obj)
            return {
            'statusCode': 200,
            'headers':{'Access-Control-Allow-Origin':'*'},
            'body': json.dumps(arr)
            }
        elif 'all' in body:
            types=[]
            if 'it' in body:
                for item in json.loads(body['it']):
                    types.append(item)
            sql = """select item_container_history.container_id,
                    count (item_type.type_id) as number_of_items,
                    item_type.type, item_type.type_id
                    from item_container_history, items, item_type
                    where (item_container_history.item_id=items.item_id and items.type_id=item_type.type_id)
                    group by item_container_history.container_id, item_type.type_id
                    order by item_container_history.container_id
                    """
            results = psql_get(sql)
            arr = []
            for result in results:
                container_id, count, item_type, type_id= result
                if item_type in types:
                    obj = {
                        "Container id": container_id,
                        "Count": count,
                        "Item Type": item_type
                    }
                    arr.append(obj)
            return {
            'statusCode': 200,
            'headers':{'Access-Control-Allow-Origin':'*'},
            'body': json.dumps(arr)
            }
        elif 'im' in body:
            sql = """SELECT items.item_id, item_type.type, items.expiry_date, devices.mac_address
                    FROM items
                    INNER JOIN item_type ON items.type_id = item_type.type_id
                    LEFT JOIN device_item ON items.item_id = device_item.item_id
                    LEFT JOIN devices ON devices.device_id = device_item.device_id
                    ORDER BY items.item_id ASC
                    """
            results = psql_get(sql)
            arr = []
            for result in results:
                item_id, item_type, expiry_date, mac_address = result
                obj = {
                        "Item id": item_id,
                        "Type": item_type,
                        "Expiry date": expiry_date.strftime("%m/%d/%Y, %H:%M:%S") if expiry_date else None,
                        "Device address": mac_address
                    }
                arr.append(obj)
            return {
            'statusCode': 200,
            'headers':{'Access-Control-Allow-Origin':'*'},
            'body': json.dumps(arr)
            }
        else:
            return {
            'statusCode': 400,
            'headers':{'Access-Control-Allow-Origin':'*'},
            'body': json.dumps("you must provide an id for the container (/ui/container/alarms?id=1)")
            }
    elif path == '/ui/container/items/thresholds':
        body = event['body']

        if "add" in body:
            add_row = body["add"]
            container_id = add_row['container_id']
            type_id = add_row['type_id']
            amber = add_row['amber']
            red = add_row['red']
            # add new container
            sql_check = """ SELECT * FROM container_stock_thresholds
                WHERE container_id = {}
                AND item_type_id = {}
                """.format(container_id,type_id)
            if len(psql_get(sql_check)) > 0:
                return {
                'statusCode': 403,
                'headers':{'Access-Control-Allow-Origin':'*'},
                'body': json.dumps("Item already exists")
                }
            sql = """INSERT INTO container_stock_thresholds(amber_th, red_th, container_id, item_type_id)
            VALUES ({}, {}, {}, {});
            """.format(amber, red, container_id, type_id)
        elif "update" in body:
            # update container
            add_row = body["update"]
            id = add_row['id']
            amber = add_row['amber']
            red = add_row['red']
            sql = """UPDATE container_stock_thresholds
            SET amber_th = {},
                red_th = {}
                WHERE container_stock_th_id = {};
            """.format(amber, red, id)
        else :
            return {
            'statusCode': 400,
            'headers':{'Access-Control-Allow-Origin':'*'},
            'body': json.dumps("add or update body needed")
            }

        psql_put(sql)
        return {
            'statusCode': 200,
            'headers':{'Access-Control-Allow-Origin':'*'},
            'body': json.dumps("thresholds added")
            }
    elif path == '/ui/container/access_history':
        body = event['queryStringParameters']
        if 'id' in body:
            clauses = []
            if 't1' in body:
                clauses.append("AND container_access.timestamp BETWEEN '{}' AND '{}'".format(body['t1'], body['t2']))

            sql = """SELECT container_access.container_id, users.name, container_access.access_type, container_access.timestamp
                    FROM container_access LEFT OUTER JOIN users ON (container_access.user_id = users.user_id)
                    WHERE container_id = {}
                 """.format(body['id'])
            for clause in clauses:
                sql = sql + clause
            sql = sql +  ' ORDER BY timestamp DESC'
            results = psql_get(sql)
            arr = []
            for result in results:
                container_id, name, access_type, timestamp= result
                obj = {
                    "Name": name,
                    "Access": "Entry" if access_type else "Exit",
                    "Timestamp": timestamp.strftime("%m/%d/%Y, %H:%M:%S")
                }
                arr.append(obj)
            return {
            'statusCode': 200,
            'headers':{'Access-Control-Allow-Origin':'*'},
            'body': json.dumps(arr)
            }
        else:
            return {
            'statusCode': 400,
            'headers':{'Access-Control-Allow-Origin':'*'},
            'body': json.dumps("you must provide an id for the container (/ui/container/alarms?id=1)")
            }

    elif path == '/ui/container/orders':
        body = event['queryStringParameters']
        if 'id' in body:
            clauses = []
            if 't1' in body:
                clauses.append(" AND itd.eta BETWEEN '{}' AND '{}'".format(body['t1'], body['t2']))

            sql = """
                    SELECT itd.leadtime, itd.timestamp_request, itd.eta, itt.type, itd.quantity FROM item_delivery AS itd
                    INNER JOIN items AS it ON itd.item_id = it.item_id
                    INNER JOIN item_type AS itt ON it.type_id = itt.type_id
                    WHERE itd.container_id = {}
                 """.format(body['id'])
            for clause in clauses:
                sql = sql + clause
            sql = sql +  ' ORDER BY itd.eta ASC'
            results = psql_get(sql)
            arr = []
            for result in results:
                leadtime, ts, eta, item_type, quant = result
                obj = {
                    "Leadtime": leadtime,
                    "Request timestamp": ts.strftime("%m/%d/%Y, %H:%M:%S"),
                    "Eta": eta.strftime("%m/%d/%Y, %H:%M:%S"),
                    "Item type": item_type,
                    "Quantity": quant
                }
                arr.append(obj)
            return {
            'statusCode': 200,
            'headers':{'Access-Control-Allow-Origin':'*'},
            'body': json.dumps(arr)
            }
        else:
            return {
            'statusCode': 400,
            'headers':{'Access-Control-Allow-Origin':'*'},
            'body': json.dumps("you must provide an id for the container (/ui/container/alarms?id=1)")
            }
    elif path == '/ui/users':
        body = event['queryStringParameters']
        sql ="""SELECT users.user_id, users.name, users.username, users.accesscard, user_role.type, users.active FROM users
            INNER JOIN user_role ON users.role_id=user_role.role_id
            """
        results = psql_get(sql)
        arr = []
        for result in results:
            user_id, name, username, accesscard, user_type, active = result
            obj = {
                "Id": user_id,
                "Name": name,
                "Email": username,
                "Accesscard": accesscard,
                "Role": user_type,
                "Active": active
            }
            arr.append(obj)
        return {
        'statusCode': 200,
        'headers':{'Access-Control-Allow-Origin':'*'},
        'body': json.dumps(arr)
        }
    elif path == '/ui/users/add':
        body = json.loads(event['body'])
        if body['id'] == 0:
            # does this user already exist
            sql_check = """SELECT * FROM users
            WHERE username = '{}'
            """.format(body['email'])

            if len(psql_get(sql_check))>0:
                return {
                    'statusCode': 403,
                    'headers':{'Access-Control-Allow-Origin':'*'},
                    'body': json.dumps('User Already Exist')
                }

            # add new user
            sql = """INSERT INTO users(name, username, password, accesscard, role_id, active)
            VALUES ('{}', '{}', '{}', '{}', {}, {});
            """.format(body['name'], body['email'], body['pass'], body['accesscard'], body['role'], True)

        elif 'pass' in body:
            # update user
            sql = """UPDATE users
            SET name = '{}',
                username = '{}',
                password = '{}',
                accesscard = '{}',
                role_id = {}
                WHERE user_id = {};
            """.format(body['name'], body['email'], body['pass'], body['accesscard'],  body['role'], body['id'])
        else:
            # update user
            sql = """UPDATE users
            SET name = '{}',
                username = '{}',
                accesscard = '{}',
                role_id = {}
                WHERE user_id = {};
            """.format(body['name'], body['email'], body['accesscard'], body['role'], body['id'])

        psql_put(sql)

        return {
        'statusCode': 200,
        'headers':{'Access-Control-Allow-Origin':'*'},
        'body': json.dumps("users updated")
        }

    elif path == '/ui/users/update':
        body = json.loads(event['body'])
        if 'state' in body:
            sql = """UPDATE users
            SET active = {}
                WHERE user_id = {};
            """.format(body['state'], body['id'])
            psql_put(sql)
            return {
            'statusCode': 200,
            'headers':{'Access-Control-Allow-Origin':'*'},
            'body': json.dumps("User state updated")
            }

    elif path == '/ui/container/thresholds':
        method = event['httpMethod']
        if method == "GET":
            body = event['queryStringParameters']
            sql ="""
                    select container_th_id, container_id, temp_min, temp_max, humid_min, humid_max, shock_th, light_th
                    from container_condition_thresholds
                    where container_id = {};
                """.format(body['id'])
            results = psql_get(sql)
            arr = []
            for result in results:
                th_id, container_id, temp_min, temp_max, humid_min, humid_max, shock_th, light_th= result
                obj = {
                    "Id": th_id,
                    "Temperature (min)": temp_min,
                    "Temperature (max)": temp_max,
                    "Humidity (min)": humid_min,
                    "Humidity (max)": humid_max,
                    "Shock threshold": shock_th,
                    "Light threshold": light_th
                }
                arr.append(obj)
            return {
            'statusCode': 200,
            'headers':{'Access-Control-Allow-Origin':'*'},
            'body': json.dumps(arr)
            }
        elif method == "POST":
            body = event['body']['update']
            sql = """UPDATE container_condition_thresholds
            SET temp_min = {},
            temp_max = {},
            humid_min = {},
            humid_max = {}
            WHERE container_th_id = {}
            """.format(body["tmin"], body["tmax"], body["hmin"], body["hmax"], body['id'])
            psql_put(sql)
            return {
            'statusCode': 200,
            'headers':{'Access-Control-Allow-Origin':'*'},
            'body': json.dumps("updated")
            }
    elif path == '/ui/items/types':
        sql ="""
                SELECT * FROM item_type
            """
        results = psql_get(sql)
        arr = []
        for result in results:
            id, type, incompatibility, tmin, tmax, hmin, hmax, shock, description = result
            obj = {
                "id": id,
                "type": type,
                "incompatibility": incompatibility,
                "tmin": tmin,
                "tmax": tmax,
                "hmin": hmin,
                "hmax": hmax,
                "shock": shock,
                "description": description
            }
            arr.append(obj)
        return {
        'statusCode': 200,
        'headers':{'Access-Control-Allow-Origin':'*'},
        'body': json.dumps(arr)
        }
    elif path == '/ui/items/types/add':
        body = json.loads(event['body'])
        if body['id'] == 0:
            # add new item type
            sql = """INSERT INTO item_type(type, incompatibility, temp_min, temp_max, humid_min, humid_max, shock, description)
            VALUES ('{}', '{}', {}, {}, {}, {}, {}, '{}');
            """.format(body['t'], body['i'], body['mint'], body['maxt'], body['minh'], body['maxh'], body['s'], body['d'])
        else:
            # update container
            sql = """UPDATE item_type
            SET type = '{}',
            incompatibility = '{}',
            temp_min = {},
            temp_max = {},
            humid_min = {},
            humid_max = {},
            shock = {},
            description = '{}'
                WHERE type_id = {};
            """.format(body['t'], body['i'], body['mint'], body['maxt'], body['minh'], body['maxh'], body['s'], body['d'], body['id'])

        psql_put(sql)

        return {
        'statusCode': 200,
        'headers':{'Access-Control-Allow-Origin':'*'},
        'body': json.dumps("container updated")
        }
    elif path == '/ui/items/item/add':
        body = json.loads(event['body'])
        if body['id'] == 0:
            sql = """SELECT type_id FROM item_type WHERE type = '{}'
            """.format(body['item'])
            item_type = psql_get(sql)[0]
            sql = """INSERT INTO items(expiry_date, alarm, type_id)
            VALUES ('{}', false, {})
            """.format(body['expiryDate'], item_type)
            item_id = psql_put(sql)
            sql = """SELECT device_id FROM devices WHERE mac_address = '{}'
            """.format(body['device'])
            device_id = psql_get(sql)[0]
            sql = """INSERT INTO device_item(device_id, item_id, timestamp_add, timestamp_remove)
            VALUES({}, {}, '{}', null)
            """.format(device_id, item_id, body['timestamp'])
            psql_put(sql)

        else:
            sql = """SELECT type_id FROM item_type WHERE type = '{}'
            """
            type_id = psql_get(sql)[0]

            sql = """
            UPDATE items
            SET type_id = {},
            expiry_date = '{}'
            WHERE item_id = {}
            """.format(type_id, body['expiryDate'], body['id'])
            psql_put(sql)
            sql = """SELECT device_id FROM devices WHERE mac_address = '{}'""".format(body['device'])
            device_id = psql_get(sql)[0]
            sql = """UPDATE device_item
            SET device_id = {}
            WHERE item_id = {}
            """.format(device_id, body['id'])
            psql_put(sql)

        return {
        'statusCode': 200,
        'headers':{'Access-Control-Allow-Origin':'*'},
        'body': json.dumps("inserted")
        }
    elif path == '/ui/items':
        sql = """SELECT i.item_id, i.expiry_date, i.alarm, it.type, d.mac_address
        FROM items as i
        LEFT JOIN item_type as it ON i.type_id=it.type_id
        LEFT JOIN device_item as di ON i.item_id=di.item_id
        LEFT JOIN devices as d ON di.device_id=d.device_id
        """
        results = psql_get(sql)
        arr = []
        for result in results:
            item_id, expiryDate, alarm, type, mac_address = result
            obj = {
                "Id": item_id,
                "Type": type,
                "Device": mac_address,
                "Expiry Date": expiryDate.strftime("%m/%d/%Y, %H:%M:%S"),
                "Alarm raised": alarm
            }
            arr.append(obj)
        return {
        'statusCode': 200,
        'headers':{'Access-Control-Allow-Origin':'*'},
        'body': json.dumps(arr)
        }

    elif path == '/ui/devices':
        sql ="""
                SELECT * FROM devices
                WHERE device_id NOT IN (
                	SELECT device_id FROM device_item
                )
            """
        results = psql_get(sql)
        arr = []
        for result in results:
            device_id, mac_address, device_type_id = result
            obj = {
                "id": device_id,
                "mac": mac_address
            }
            arr.append(obj)
        return {
        'statusCode': 200,
        'headers':{'Access-Control-Allow-Origin':'*'},
        'body': json.dumps(arr)
        }
    elif path == '/ui/users/roles':
        sql ="""
                SELECT * FROM user_role
            """
        results = psql_get(sql)
        arr = []
        for result in results:
            device_id, type = result
            obj = {
                "id": device_id,
                "type": type
            }
            arr.append(obj)
        return {
        'statusCode': 200,
        'headers':{'Access-Control-Allow-Origin':'*'},
        'body': json.dumps(arr)
        }

    body = json.loads(event['body'])
    if "task" in body:
        tasks = body['task'].split('/')
        if tasks[0] == "container_history":
            if tasks[1] == "put":
                # put data into container history

                humidity = body['metrics']['humidity']
                temperature = body['metrics']['temperature']


                sql_string = "insert into container_history(container_id, timestamp, humidity, temperature) values(1, NOW(), %s, %s)" % (humidity, temperature)
                psql_put(sql_string)
    elif path == '/api/assets':
        body = json.loads(event['body'])
        mac = body['mac']
        timestamp = body['timestamp']

        sql = """
        """

    else:
        for message in body:
            # message = json.loads(message)
            if all(key in message for key in ['mac','rawData', 'timestamp']):
                timestamp = message['timestamp']
                mac = message['mac']
                mac_white_list = [
                    "F54853CC92D7",
                    "CEE6DEEC159A",
                    "AC233FA9DC85",
                    "546C0E5330D1",
                    "546C0E533154",
                    "AC233FC0226E",
                    "CEE6DEEC159A"
                    ]
                if mac not in mac_white_list:
                    continue
                raw_data = message['rawData']
                obj = {
                    "timestamp": timestamp,
                    "mac": mac,
                    "raw_data": raw_data
                }

                sql_string = "insert into container_history(container_id, timestamp, mac, raw_data) values(1, NOW(), '%s', '%s')" % (mac, raw_data)
                psql_put(sql_string)

    return {
        'statusCode': 200,
        'headers':{'Access-Control-Allow-Origin':'*'},
        'body': json.dumps(body)
    }
