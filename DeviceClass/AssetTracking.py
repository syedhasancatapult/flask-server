import json
from Database.Postgres import Postgres
from DeviceClass import Helpers
import os
from dotenv import load_dotenv


class AssetTracking:

    def __init__(self, assets, host='localhost', port=5432, db='das', username='postgres', password='password'):

        dotenv_path = '/home/das/flask-server/.env'
        load_dotenv(dotenv_path)
        self.container_id = os.environ.get("CONTAINER_ID")
        self.db = Postgres(host=host, port=port, dbname=db, username=username, password=password)
        try:
            self.mac = assets['addr'].upper()
            self.timestamp = assets['timestamp']
        except KeyError as e:
            raise Exception(str(e) + " is missing from the payload : " + json.dumps(assets))

        # check if the mac is in DB
        if not self.__is_device_known():
            raise Exception("The mac address {} is not a device registered in DAS".format(self.mac))
        self.item_id, self.type_id = self.get_item_id_type_id_from_mac()

        if self.is_item_in_container():
            """update where item_id = self.item_id item_container_history"""
            self.update_item_record()
        else:
            """insert into item_container_history"""
            self.insert_item_record()
            self.unexpected_item_check()

        self.incompatibility_check()

    def get_item_id_type_id_from_mac(self) -> tuple:
        sql = """SELECT i.item_id, d.mac_address, it.type_id, it.type FROM items AS i
        JOIN device_item AS di ON i.item_id = di.item_id
        JOIN item_type as it ON i.type_id = it.type_id
        JOIN devices AS d ON d.device_id = di.device_id
        WHERE d.mac_address = '{}'
        """.format(self.mac)
        item_id, mac, type_id, type_name = self.db.get_one(sql)
        return item_id, type_id

    def is_item_in_container(self) -> bool:
        sql = """SELECT * FROM item_container_history AS ich
        WHERE item_id = {}
        AND checked_in_out = 'true'
        """.format(self.item_id)
        return len(self.db.get(sql)) > 0

    def update_item_record(self):
        sql = """UPDATE item_container_history
        SET last_seen_ts = '{}'
        WHERE item_id = {}
        AND checked_in_out = 'true'
        """.format(self.timestamp, self.item_id)
        self.db.put(sql)

    def insert_item_record(self):

        sql = """INSERT INTO item_container_history(checked_in_out, check_in_ts, item_id, container_id, last_seen_ts)
        VALUES (True, '{}', {}, {}, '{}')
        """.format(self.timestamp, self.item_id, int(self.container_id), self.timestamp)
        self.db.put(sql)

    def unexpected_item_check(self):
        sql_item = """SELECT item_id, type_id FROM items where item_id = {}""".format(self.item_id)
        item_id, type_id = self.db.get(sql_item)[0]
        sql = """SELECT container_stock_th_id, item_type_id FROM container_stock_thresholds
                    """
        results = self.db.get(sql)
        current_types = []
        for result in results:
            stock_id, stock_type_id = result
            current_types.append(stock_type_id)
        print("############## Current Types #############")
        print(current_types)
        if type_id not in current_types:
            # raise_alarm
            alarm_string = """
            Item {} has appeared in the container and should not be here, Define a threshold for this item
            """.format(Helpers.get_item_type_by_id(type_id))
            Helpers.raise_item_stock_alarm(item_id, alarm_string, self.timestamp, int(self.container_id))

    def incompatibility_check(self):
        print('lets try to check if this item is the correct item')

        # get item using device
        item_to_check = Helpers.get_item_incompatibility_by_item_id(self.item_id)

        stock = Helpers.get_containers_current_item_incompatibility()
        for item_stock in stock:
            if item_to_check['type_id'] in item_stock['list']:
                item_type = Helpers.get_item_type_by_id(item_to_check['type_id'])
                item_type2 = Helpers.get_item_type_by_id(item_stock['type_id'])
                alarm_string = """{}. Item {} ({}), is not compatible with {} which is already in the container
                """.format(item_to_check["item_history_id"], item_to_check['item_id'], item_type, item_type2)
                if Helpers.get_incompatibility_alarm_by_ich(item_to_check["item_history_id"]) == 0:
                    Helpers.raise_item_incompatibility_alarm(
                        item_to_check['item_id'],
                        alarm_string,
                        self.timestamp,
                        int(self.container_id)
                    )
            if item_stock['type_id'] in item_to_check['list']:
                item_type = Helpers.get_item_type_by_id(item_stock['type_id'])
                item_type2 = Helpers.get_item_type_by_id(item_to_check['type_id'])
                alarm_string = """{}. Item {} ({}), is not compatible with {} which has just entered into the container
                """.format(item_stock["item_history_id"], item_stock['item_id'], item_type, item_type2)

                if Helpers.get_incompatibility_alarm_by_ich(item_stock["item_history_id"]) == 0:
                    Helpers.raise_item_incompatibility_alarm(
                        item_to_check['item_id'],
                        alarm_string,
                        self.timestamp,
                        int(self.container_id)
                    )

    def __is_device_known(self) -> bool:
        sql = """SELECT device_id, mac_address FROM devices WHERE mac_address = '{}'
        """.format(self.mac)
        return False if len(self.db.get(sql)) == 0 else True