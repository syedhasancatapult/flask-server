import os
from dotenv import load_dotenv
import json
from Database.Postgres import Postgres
from DeviceClass.AccessTypes import AccessTypes
from DeviceClass import Helpers
import time
from DeviceClass.AlarmTypes import AlarmType
import logging


class ConditionMonitoring:
    def __init__(self, payload):

        self.device_type = None
        dotenv_path = '/home/das/flask-server/.env'
        load_dotenv(dotenv_path)
        self.container_id = os.environ.get("CONTAINER_ID")

        try:
            self.mac = payload['addr'].upper()
            self.timestamp = payload['timestamp']
            self.parsed_data = payload['parsed_data']

        except KeyError as e:
            raise KeyError(str(e) + " is missing from the payload : " + json.dumps(payload))


    def set_device_type(self):

        with open('/home/das/flask-server/DeviceClass/devices.txt') as file:

            devices = file.readlines()
            for device in devices:

                name = device.split(' ')[0]
                mac = device.split(' ')[1].strip('\n')

                if self.mac == mac:
                    self.device_type = name
                    return name

            raise Exception("Device Type unknown {}".format(self.mac))

    def raise_alarm(self, alarm_string, alarm_type):
        cloud = Postgres()
        # insert into alarms table
        sql = """INSERT INTO alarms (timestamp, description, container_id, alarm_type_id, alarm_active_notactive)
        VALUES ('{}', '{}', {}, {}, true)
        """.format(
            self.timestamp,
            alarm_string,
            self.container_id,
            alarm_type
        )
        cloud.put(sql)

    def cm_alarm_update_inject(self, description, alarm_type, only_prefix:bool):
        cloud = Postgres()

        if only_prefix:
            description_prefix = description.split(" ")[0] + " " + description.split(" ")[1]
        else:
            description_prefix = description

        query_to_find_alarm = """SELECT alarm_id, timestamp FROM alarms
        WHERE description LIKE '{} %' 
        AND alarm_active_notactive = 'true' 
        AND alarm_type_id = {}
        ORDER BY timestamp DESC 
        LIMIT 1""".format(description_prefix, alarm_type)

        try:
            alarm_id, timestamp = cloud.get_one(query_to_find_alarm)
        except TypeError:
            alarm_id = None

        if not alarm_id:
            """Insert"""
            self.raise_alarm(description, alarm_type)
        else:
            """Update"""
            update_alarm_sql = """ UPDATE alarms
            SET description = '{}',
            timestamp = '{}'
            WHERE alarm_id = {}
            """.format(description, self.timestamp, alarm_id)
            cloud.put(update_alarm_sql)

    def update_container_access(self, container_access_id, user_id):
        db = Postgres()
        sql = """UPDATE container_access
        SET user_id = {}
        WHERE container_access_id = {}
        """.format(user_id, container_access_id)
        db.put(sql)

    def get_user_from_access_card(self, access_card):
        db = Postgres()
        user_id, accesscard = db.get_one("SELECT user_id, accesscard FROM users WHERE accesscard = '{}'".format(access_card))
        return user_id

    def set_container_access(self, access_type, timestamp, user_id):
        db = Postgres()

        # user has entered container without swiping do INSERT
        if not user_id:
            sql = """INSERT INTO container_access(access_type, timestamp, user_id, container_id)
            VALUES('{}', '{}', {}, {})
            """.format(
                    access_type,
                    timestamp,
                    "null",
                    int(self.container_id)
                )
            db.put(sql)
            return "inserted"
        else:
            sql = """INSERT INTO container_access(access_type, timestamp, user_id, container_id)
            VALUES('{}', '{}', {}, {})
            """.format(
                    access_type,
                    timestamp,
                    user_id,
                    int(self.container_id)
                )
            db.put(sql)
            return "inserted"

    def check_thresholds(self, t, h, s):
        # get condition_monitoring thresholds for this container
        db = Postgres()
        sql = """SELECT * FROM container_condition_thresholds
        WHERE container_id = {}
        """.format(int(self.container_id))
        results = db.get(sql)
        th_id, tmin, tmax, hmin, hmax, sth, lth, cid, bth = results[0]

        if t > tmax:
            alarm = """temperature above threshold {} in container {} at {}, temperature value: {}""". format(
                tmax, int(self.container_id), self.timestamp, t
            )
            self.cm_alarm_update_inject(alarm, AlarmType.ThresholdBreached, True)
        if t < tmin:
            alarm = """temperature below threshold {} in container {} at {}, temperature value: {}""". format(
                tmin, int(self.container_id), self.timestamp, t
            )
            self.cm_alarm_update_inject(alarm, AlarmType.ThresholdBreached, True)

        if h > hmax:
            alarm = """humidity above threshold {} in container {} at {}, humidity value: {}""". format(
                hmax, int(self.container_id), self.timestamp, h
            )
            self.cm_alarm_update_inject(alarm, AlarmType.ThresholdBreached, True)

        if h < hmin:
            alarm = """humidity below threshold {} in container {} at {}, humidity value: {}""". format(
                hmin, int(self.container_id), self.timestamp, h
            )
            self.cm_alarm_update_inject(alarm, AlarmType.ThresholdBreached, True)

        if s > sth:
            alarm = """shock breach in container {} at {}, shock value: {}""". format(
                int(self.container_id), self.timestamp, s
            )
            self.cm_alarm_update_inject(alarm, AlarmType.ThresholdBreached, True)

        item_types_temp = Helpers.temp_out_of_range_item_type(t)
        item_types_humid = Helpers.humid_out_of_range_item_type(h)

        for item_type in item_types_temp:
            type_id, type_name, type_tmin, type_tmax, type_hmin, type_hmax = item_type
            self.cm_alarm_update_inject(
                alarm_type=AlarmType.ThresholdBreached,
                description="{} items have breached the temperature threshold".format(type_name),
                only_prefix=False
            )
            Helpers.set_item_type_alarm_state(type_id, True)

        for item_type in item_types_humid:
            type_id, type_name, type_tmin, type_tmax, type_hmin, type_hmax = item_type
            self.cm_alarm_update_inject(
                alarm_type=AlarmType.ThresholdBreached,
                description="{} items have breached the humidity threshold".format(type_name),
                only_prefix=False
            )
            Helpers.set_item_type_alarm_state(type_id, True)

    def get_gps(self):
        # --select the gps interface

        stream = os.popen('gpspipe -w -n 5')
        output = stream.read()
        list = output.split('\n')
        lat = None
        lon = None
        alt = None
        for item in list:
            item = json.loads(item)
            if item['class'] == "TPV" and item['mode'] == 3:
                lat = item['lat']
                lon = item['lon']
                alt = item['alt']
                break

        return lat,lon,alt

    def get_shock(self):
        number_of_lines = 0
        try:
            with open("/home/das/shock.txt", "r+") as f:
                shock_lines = f.readlines()
                number_of_lines = len(shock_lines)
                f.write("")
        except Exception as e:
            print(e)

        return number_of_lines

    def send(self):
        dotenv_path = '/home/das/flask-server/.env'
        load_dotenv(dotenv_path)

        db = Postgres()
        if self.device_type == 'condition_monitoring':

            logging.info(self.parsed_data)

            light = self.parsed_data.get("illumination", "null")
            ts = self.timestamp
            temp = self.parsed_data.get("temperature", "null")
            humid = self.parsed_data.get("humidity", "null")
            shock = self.get_shock()
            battery_level = "null"
            lat = self.parsed_data.get("lat", "null")
            long = self.parsed_data.get("lon", "null")
            altitude = self.parsed_data.get("alt", "null")

            logging.info(altitude)


            sql = """INSERT INTO container_history(light, timestamp, container_id, gps_lat, gps_long, temperature, humidity, shock, altitude, battery_level)
            VALUES ('{}', '{}', {}, {}, {}, {}, {}, {}, {}, {})
            """.format(
                light, ts, int(self.container_id), lat, long, temp, humid, shock, altitude, battery_level
                )

            logging.info(sql)
            db.put(sql)

            if temp is not "null":
                self.check_thresholds(temp, humid, shock)

            return "yes"

        if self.device_type == 'door':

            door_open = False if self.parsed_data['door_state'] == "closed" else True

            if door_open:

                door_db = Postgres()

                last_entry_sql = """SELECT access_type, timestamp FROM container_access
                ORDER BY timestamp DESC
                LIMIT 1
                """
                result, timestamp = door_db.get_one(last_entry_sql)
                if not result:
                    self.set_container_access(True, self.timestamp, None)

                    # wait one min for a tap
                    door_mac = os.environ.get("DOOR")
                    if door_mac != "NODOOR":
                        time.sleep(60)

                        last_user_sql = """SELECT user_id, timestamp FROM container_access
                        WHERE access_type = 'true'
                        ORDER BY timestamp DESC
                        LIMIT 1
                        """
                        result, timestamp = door_db.get_one(last_user_sql)
                        if not result:
                            alarm = "Unauthorised access into container"
                            self.raise_alarm(alarm, 2)
                        if result is not None:
                            alarm = "No alarm"

                return "door_open"

            else:
                last_user_sql = """SELECT container_access_id, user_id, access_type
                FROM container_access
                ORDER BY timestamp DESC LIMIT 1
                """
                result = db.get_one(last_user_sql)
                if result is not None:
                    container_access_id, user_id, access_type = result
                    if access_type:

                        sql = """INSERT INTO container_access (access_type, timestamp, user_id, container_id)
                        VALUES (false, now(), {}, {})
                        """.format('null' if not user_id else user_id, int(self.container_id))
                        db.put(sql)
                        return "log_out"
                return "door_closed"

        if self.device_type == "cardreader":

            sql_last_entry_row = """SELECT container_access_id, user_id
            FROM container_access 
            WHERE access_type = 'true'
            ORDER BY timestamp DESC 
            LIMIT 1
            """

            container_access_id, user_id = db.get_one(sql_last_entry_row)

            if not user_id:
                '''
                There is currently no user associated with the entry
                '''
                self.update_container_access(
                    container_access_id,
                    self.parsed_data['UID']
                )
            else:
                '''
                There is currently a user associated with the entry
                Checking if the user is the same
                '''
                if user_id == self.parsed_data["UID"]:
                    '''
                    Same user has tapped in again, so we can UPDATE this users timestamp
                    '''
                    self.update_container_access(
                        container_access_id,
                        self.parsed_data['UID']
                    )
                else:
                    '''
                    A different user has tapped in lets INSERT them in the table 
                    '''
                    self.set_container_access(
                        True,
                        self.timestamp,
                        self.parsed_data['UID']
                    )
            return "card_read"
