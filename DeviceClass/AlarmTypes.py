from enum import IntEnum


class AlarmType(IntEnum):
    Incompatibility = 1
    UnauthorisedAccess = 2
    StockLevels = 3
    ThresholdBreached = 4
    UnexpectedItem = 5
