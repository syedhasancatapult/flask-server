import os
import json


class GPS:
    def __init__(self):
        self.latitude = None
        self.longitude = None
        self.altitude = None

    def cold_start_gps(self):
        """
        This class lets the user restart the GPS module
        :return:
        """
        pass

    def hot_start_gps(self):
        """
        This call will hot start the GPs module
        :return:
        """
        pass

    def stop_gps(self):
        pass

    def get_latest_fix(self) -> dict:
        for item in os.popen('gpspipe -w -n 5').read().split('\n'):
            if not item :
                continue
            item = json.loads(item)
            if item['class'] == "TPV" and item['mode'] in [2,3] :
                self.latitude = item['lat']
                self.longitude = item['lon']
                self.altitude = item['alt']
                return item

        return {}
