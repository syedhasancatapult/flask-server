ALTER TABLE public.container_history
    ALTER COLUMN gps_lat DROP NOT NULL;

ALTER TABLE public.container_history
    ALTER COLUMN gps_long DROP NOT NULL;

ALTER TABLE public.container_history
    ALTER COLUMN temperature DROP NOT NULL;

ALTER TABLE public.container_history
    ALTER COLUMN humidity DROP NOT NULL;

ALTER TABLE public.container_history
    ALTER COLUMN shock DROP NOT NULL;

ALTER TABLE public.container_history
    ALTER COLUMN light DROP NOT NULL;