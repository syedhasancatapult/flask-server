const urlSearchParams = new URLSearchParams(window.location.search);
const params = Object.fromEntries(urlSearchParams.entries());
var id = params['id'];
const Http = new XMLHttpRequest();
const url='http://192.168.0.52/ui/container/metrics?id=' + id;
var header = document.getElementById("header-ccm");
header.innerText = "Container " + id + "  Conditions";

function menu(page) {
  if (page == 'containers') {
    location.href = "demo.html"
  }
  else if (page == 'container menu') {
    location.href = "demo-selected-container.html?id=" + id;
  }
}

function plotTemp(data) {

  var data1 = [];
  console.log(data);
  for(row in data) {
    arr = [];
    var timeInMillis = new Date(data[row]['Timestamp']).getTime();
    arr[0] = timeInMillis;
    arr[1] = data[row]['Temperature (C)'];
    data1.push(arr);
  }
  console.log(data1);

  $(document).ready(function(){
    $.plot($("#temperature"), [
        {
            data: data1,
            label: "Temperature (C)",
            lines: { show: true}
        }
    ],{
      xaxis: {
        mode: 'time',
        timeformat: '%Y-%m-%d %H:%M:%S'
      }
    });

});
}

function plotHumid(data) {

  var data1 = [];
  console.log(data);
  for(row in data) {
    arr = [];
    var timeInMillis = new Date(data[row]['Timestamp']).getTime();
    arr[0] = timeInMillis;
    arr[1] = data[row]['Humidity (%)'];
    data1.push(arr);
  }
  console.log(data1);

  $(document).ready(function(){
    $.plot($("#humidity"), [
        {
            data: data1,
            label: "Humidity (%)",
            lines: { show: true}
        }
    ],{
      xaxis: {
        mode: 'time',
        timeformat: '%Y-%m-%d %H:%M:%S'
      }
    });

});
}

function plotShock(data) {

  var data1 = [];
  console.log(data);
  for(row in data) {
    arr = [];
    var timeInMillis = new Date(data[row]['Timestamp']).getTime();
    arr[0] = timeInMillis;
    arr[1] = data[row]['Shock (%)'];
    data1.push(arr);
  }
  console.log(data1);

  $(document).ready(function(){
    $.plot($("#shock"), [
        {
            data: data1,
            label: "Shock (%)",
            lines: { show: true}
        }
    ],{
      xaxis: {
        mode: 'time',
        timeformat: '%Y-%m-%d %H:%M:%S'
      }
    });

});
}


function generateTableHead(table, data) {
  let thead = table.createTHead();
  let row = thead.insertRow();
  for (let key of data) {
    let th = document.createElement("th");
    let text = document.createTextNode(key);
    th.appendChild(text);
    row.appendChild(th);
  }
}

function generateTable(table, data) {
  for (let element of data) {
    let row = table.insertRow();

    for (key in element) {
      let cell = row.insertCell();
      let text = document.createTextNode(element[key]);
      cell.appendChild(text);
    }
  }
}

Http.open("GET", url);
Http.send();

Http.onreadystatechange = (e) => {
  if(Http.readyState === XMLHttpRequest.DONE) {
    response = JSON.parse(Http.responseText);

    let table = document.getElementById("table-containers");
    let data = Object.keys(response[0]);
    generateTableHead(table, data);
    generateTable(table, response);
    plotTemp(response);
    plotHumid(response);
    plotShock(response);
  }

}
