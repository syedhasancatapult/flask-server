const urlSearchParams = new URLSearchParams(window.location.search);
const params = Object.fromEntries(urlSearchParams.entries());
var id = params['id'];
const Http = new XMLHttpRequest();
const url='http://192.168.0.52/ui/items';
var qs = "?im=0";
var initFlag = true;
var selectedRow = 0;

getRequest();

$('#item-type-select-box').select2();

$('#signup-modal').on('hidden.bs.modal', function () {
    selectedRow = 0;
    $('#item-select').val(null).trigger('change');
    $("#expiry-date-picker").val('');
    $('#device-select').val(null).trigger('change');
});

document.getElementById("container-menu-bc").onclick = function () {
        location.href = "items.html";
};

document.getElementById("btn-item-update").onclick = function () {
    // get item type
    var item_type = $('#item-select').val();
    // get date
    var expiryDate = $("#expiry-date-picker").val();
    // get mac
    var device = $('#device-select').val();
    // send to endpoint
    var data = {
      id: selectedRow,
      item:item_type,
      expiryDate: expiryDate,
      device:device,
      timestamp:new Date().toISOString()
    }
    $.ajax({
      type: "POST",
      url: 'http://192.168.0.52/ui/items/item/add',
      data: JSON.stringify(data),
      contentType: 'application/json; charset=utf-8',
      success: function(res) {
        getRequest();
        $('#item-select').val(null).trigger('change');
        $("#expiry-date-picker").val('');
        $('#device-select').val(null).trigger('change');
        selectedRow = 0;
        $('#signup-modal').modal('hide');
      }
    });
};

function clearTable(table){
  $("#table-body").remove();
}
const Http_item_type = new XMLHttpRequest();
var type_url = "http://192.168.0.52/ui/items/types"
Http_item_type.open("GET", type_url);
Http_item_type.send();


Http_item_type.onreadystatechange = (e) => {
  if(Http_item_type.readyState === XMLHttpRequest.DONE) {
    response = JSON.parse(Http_item_type.responseText);
    console.log(response)
    for (itemType in response){
      var optgroup = document.getElementById("item-select");
      var option = document.createElement("option");
      option.innerHTML = response[itemType]["type"];
      optgroup.appendChild(option);
    }
  }
}

const Http_device = new XMLHttpRequest();
var type_url = "http://192.168.0.52/ui/devices"
Http_device.open("GET", type_url);
Http_device.send();


Http_device.onreadystatechange = (e) => {
  if(Http_device.readyState === XMLHttpRequest.DONE) {
    response = JSON.parse(Http_device.responseText);
    console.log(response)
    for (itemType in response){
      var optgroup = document.getElementById("device-select");
      var option = document.createElement("option");
      option.innerHTML = response[itemType]["mac"];
      optgroup.appendChild(option);
    }
  }
}

function openModal(item) {
  selectedRow = item['Item id']
  $('#signup-modal').modal('show');
  // inject itemtype into select2
  $('#item-select').val(item['Type']); // Select the option with a value of '1'
  $('#item-select').trigger('change'); // Notify any JS components that the value changed
  // inject expiry date if not none
  if (item['Expiry date']) {
    $("#expiry-date-picker").datepicker('setDate', item['Expiry date'].split(',')[0]);
  }
  // inject device address
  if (item['Device address']) {
    $('#device-select').val(item['Device address']); // Select the option with a value of '1'
    $('#device-select').trigger('change'); // Notify any JS components that the value changed
  }

  $('#signup-modal').modal('show');

}

function generateTableHead(table, data) {
  let thead = table.createTHead();
  let row = thead.insertRow();
  for (let key of data) {
    let th = document.createElement("th");
    let text = document.createTextNode(key);
    th.appendChild(text);
    row.appendChild(th);
  }
  let th = document.createElement("th");
  let text = document.createTextNode("Edit");
  th.appendChild(text);
  row.appendChild(th);
}
function generateTable(table, data) {
  let tbody = table.createTBody();
  tbody.setAttribute("id", "table-body");
  for (let element of data) {
    let row = tbody.insertRow();

    for (key in element) {
      let cell = row.insertCell();
      let text = document.createTextNode(element[key]);
      cell.appendChild(text);
    }
    let cell = row.insertCell();
    let btn = document.createElement("div");
    let icon = document.createElement("i");
    icon.className = "fe-edit";
    btn.appendChild(icon);
    btn.addEventListener("click", function() {
      openModal(element)
    });
    cell.appendChild(btn);

  }
}

function getRequest(){
  console.log('send a request');
  Http.open("GET", url + qs);
  Http.send();
}


Http.onreadystatechange = (e) => {
  if(Http.readyState === XMLHttpRequest.DONE) {
    response = JSON.parse(Http.responseText);
    console.log(response);

    if (response.length > 0){
      let table = document.getElementById("table-alarms");
      let data = Object.keys(response[0]);
      if (initFlag){

        generateTableHead(table, data);
        generateTable(table, response);
        initFlag = false;
      } else {
        //clear table
        clearTable(table);
        generateTable(table, response);
      }
    } else {
      clearTable();
    }

  }
}
