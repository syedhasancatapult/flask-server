function powerDown() {
      var txt;
      var r = confirm("Do you want to shutdown?");
      if (r == true) {
      $.ajax({
        url: "http://192.168.0.52/api/shutdown",
        type: 'GET',
        success: function(res){
          console.log('shutting down in one minute');
          console.log(res);
          alert(res);

        }
      });
      }
    };
function reboot() {
      var txt;
      var r = confirm("Do you want to reboot?");
      if (r == true) {
      $.ajax({
        url: "http://192.168.0.52/api/reboot",
        type: 'GET',
        success: function(res){
          console.log('rebooting in one minute');
          console.log(res);
          alert(res);

        }
      });
      }
    };

function sync() {
      var txt;
      var r = confirm("Do you want to sync?");
      if (r == true) {
      $.ajax({
        url: "http://192.168.0.52/api/sync",
        type: 'GET',
        success: function(res){
          console.log('Sync output');
          console.log(res);
          alert("Sync complete");
        }
      });
      }
    };

var li = document.createElement("li");

li.innerHTML =
    '<a href="#sidebarIcons" data-bs-toggle="collapse" aria-expanded="false" aria-controls="sidebarIcons"> \n' +
        '<i class="fe-cpu"></i> \n'+
        '<span> DAS System </span> \n' +
        '<span class="menu-arrow"></span> \n' +
    '</a> \n' +
    '<div class="collapse" id="sidebarIcons"> \n' +
        '<ul class="nav-second-level"> \n' +
            '<li> \n' +
                '<a href="javascript:sync()"> \n' +
                '<i class="fe-crosshair"></i> \n' +
                '<span> Sync DAS data </span> \n' +
                '</a> \n' +
            '</li> \n' +

            '<li> \n' +
                '<a href="javascript:powerDown()"> \n' +
                    '<i class="fe-power"></i> \n' +
                    '<span> Shutdown </span> \n' +
                '</a> \n' +
            ' </li> \n' +

            '<li> \n' +
                '<a href="javascript:reboot()"> \n' +
                '<i class="fe-rotate-ccw"></i> \n' +
                '<span> Reboot </span> \n' +
                '</a> \n' +
            '</li> \n' +

        '</ul> \n' +
    ' </div>';
var side_menu = document.getElementById("side-menu");
side_menu.appendChild(li);
