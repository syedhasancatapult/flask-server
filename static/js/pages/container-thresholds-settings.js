const urlSearchParams = new URLSearchParams(window.location.search);
const params = Object.fromEntries(urlSearchParams.entries());
var id = params['id'];
const Http = new XMLHttpRequest();
const url='http://192.168.0.52/ui/container/thresholds';
var qs = "?id="+id;
var qsat = "";
var qsas = "";
var qsts = "";
var initFlag = true;
var selectedRow = 0;
var current_row = {};

document.getElementById("container-menu-bc").onclick = function () {
        console.log("clicked on this element");
        location.href = "container-menu.html?id=" +id;
    };

$('#signup-modal').on('hidden.bs.modal', function () {
    selectedRow = 0;
    document.getElementById("input-tmin").value = "";
    document.getElementById("input-tmax").value = "";
    document.getElementById("input-hmin").value = "";
    document.getElementById("input-hmax").value = "";
    // document.getElementById("input-shock").value = "";
    // document.getElementById("input-light").value = "";
});

// submit button code
document.getElementById("btn-item-update").onclick = function () {
    // get item type
    // if add column
    var data = {
      update:{
        id: selectedRow,
        tmin: document.getElementById("input-tmin").value,
        tmax: document.getElementById("input-tmax").value,
        hmin: document.getElementById("input-hmin").value,
        hmax: document.getElementById("input-hmax").value,
        // shock: document.getElementById("input-shock").value,
        // light: document.getElementById("input-light").value
      }
    }

    $.ajax({
      type: "POST",
      url: 'http://192.168.0.52/ui/container/thresholds',
      data: JSON.stringify(data),
      contentType: 'application/json; charset=utf-8',
      success: function(res) {
        getRequest();
        $('#signup-modal').modal('hide');
      }
    });
};
// Edit button click constructor
document.getElementById("edit-btn").onclick = function () {
  item = current_row
  console.log(item);

  selectedRow = item['Id']
  // inject itemtype into select2
  document.getElementById("input-tmin").value = item["Temperature (min)"];
  document.getElementById("input-tmax").value = item["Temperature (max)"];
  document.getElementById("input-hmin").value = item["Humidity (min)"];
  document.getElementById("input-hmax").value = item["Humidity (max)"];
  // document.getElementById("input-shock").value = item["Shock threshold"];
  // document.getElementById("input-light").value = item["Light threshold"];


  $('#signup-modal').modal('show');

}

function clearTable(table){
  $("#table-body").remove();
}

function generateTableHead(table, data) {
  let thead = table.createTHead();
  let row = thead.insertRow();
  for (let key of data) {
    if (key == "Id"  || key == "Shock threshold" || key == "Light threshold"){
      continue;
    } else{
      let th = document.createElement("th");
      let text = document.createTextNode(key);
      th.appendChild(text);
      row.appendChild(th);
    }
  }
}
function generateTable(table, data) {
  let tbody = table.createTBody();
  tbody.setAttribute("id", "table-body");
  for (let element of data) {
    current_row = element;
    let row = tbody.insertRow();

    for (key in element) {
      if (key == "Id"  || key == "Shock threshold" || key == "Light threshold"){
        continue;
      } else{
        let cell = row.insertCell();
        let text = document.createTextNode(element[key]);
        cell.appendChild(text);
      }
    }
  }
}

function getRequest(){
  console.log('send a request');
  Http.open("GET", url + qs + qsat + qsas + qsts);
  Http.send();
}
Http.open("GET", url + qs);
Http.send();

Http.onreadystatechange = (e) => {
  if(Http.readyState === XMLHttpRequest.DONE) {
    response = JSON.parse(Http.responseText);
    console.log(response);

    if (response.length > 0){
      let table = document.getElementById("table-alarms");
      let data = Object.keys(response[0]);
      if (initFlag){

        generateTableHead(table, data);
        generateTable(table, response);
        initFlag = false;
      } else {
        //clear table
        clearTable(table);
        generateTable(table, response);
      }
    } else {
      clearTable();
    }

  }
}
