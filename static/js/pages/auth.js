const Http = new XMLHttpRequest();
const url='/api/login';


Http.onreadystatechange = (e) => {
  if(Http.readyState === XMLHttpRequest.DONE) {
    response = JSON.parse(Http.responseText);
    if (response == '200'){
      // login
      location.href = "container-map.html"
    }else if (response == '403') {
      console.log("password incorrect")
    }else if (response == '404') {
      console.log("user doesn't exist")
    }
  }
}

document.getElementById("btn-login").onclick = function () {
  // get username and Password
  email = document.getElementById("emailaddress").value;
  password = document.getElementById("password").value;
  var body = {
    "username": email,
    "password": password
  }
  // make call to api
  console.log(body);
  Http.open("POST", url, true);
  Http.setRequestHeader('Content-type', 'application/json');
  Http.send(JSON.stringify(body));
  // make decision based on reply
  };
