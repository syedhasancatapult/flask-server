const urlSearchParams = new URLSearchParams(window.location.search);
const params = Object.fromEntries(urlSearchParams.entries());
var id = params['id'];
const Http = new XMLHttpRequest();
const url='http://192.168.0.52/ui/container/items';
var qs = "?id="+id;
var qsat = "";
var qsas = "";
var qsts = "";
var initFlag = true;
var selectedRow = 0;

document.getElementById("container-menu-bc").onclick = function () {
        console.log("clicked on this element");
        location.href = "container-menu.html?id=" +id;
    };

$('#signup-modal').on('hidden.bs.modal', function () {
    selectedRow = 0;
    $('#item-select').val(null).trigger('change');
    document.getElementById("input-amber").value = "";
    document.getElementById("input-red").value = "";
    $('#item-select').prop('disabled', false);
});

// submit button code
document.getElementById("btn-item-update").onclick = function () {
    // get item type
    // if add column
    var container_id = id;
    var item_type_id = $('#item-select').select2('data')[0]['id']
    var amber = document.getElementById("input-amber").value;
    var red = document.getElementById("input-red").value;
    if (selectedRow == 0){
      var data = {
        add:{
          container_id:container_id,
          type_id:item_type_id,
          amber: amber,
          red: red
        }
      }
    } else if (selectedRow > 0){
      var data = {
        update:{
          id: selectedRow,
          amber: amber,
          red: red
        }
      }
    }

    $.ajax({
      type: "POST",
      url: 'http://192.168.0.52/ui/container/items/thresholds',
      data: JSON.stringify(data),
      contentType: 'application/json; charset=utf-8',
      success: function(res) {
        getRequest();
        $('#signup-modal').modal('hide');
      },
      error: function (jqXHR, exception) {
        alert("Item Type already exists")
      }
    });
};


function getOptId(text) {
  let id = '';
  $('#item-select').find('*').filter(function() {
    if ($(this).text() === text) {
      id = $(this).val();
    }
  });
  return id;
}
// Edit button click constructor
function openModal(item) {
  selectedRow = item['Id']
  // inject itemtype into select2
  console.log(getOptId(item['Item Type']));
  $('#item-select').val(getOptId(item['Item Type'])).change();
  $('#item-select').prop('disabled', true);
  // inject expiry date if not none
  document.getElementById("input-amber").value = item['Amber threshold'];
  document.getElementById("input-red").value = item['Red threshold'];


  $('#signup-modal').modal('show');

}

function clearTable(table){
  $("#table-body").remove();
}

function generateTableHead(table, data) {
  let thead = table.createTHead();
  let row = thead.insertRow();
  for (let key of data) {
    if (key == "Id"){
      continue;
    } else{
      let th = document.createElement("th");
      let text = document.createTextNode(key);
      th.appendChild(text);
      row.appendChild(th);
    }
  }
  let th = document.createElement("th");
  let text = document.createTextNode("Edit");
  th.appendChild(text);
  row.appendChild(th);
}
function generateTable(table, data) {
  let tbody = table.createTBody();
  tbody.setAttribute("id", "table-body");
  for (let element of data) {
    let row = tbody.insertRow();
    unexpected_item = false;
    for (key in element) {
      if (key == "Id"){
        if (element[key] == null){
            unexpected_item = true;
            element[key] = 0;
        }
        continue;
      } else{
        let cell = row.insertCell();
        var value = element[key]
        if (value==null) {
          value = 0
        }
        let text = document.createTextNode(value);
        cell.appendChild(text);

      }
    }

    if ( unexpected_item ) { row.style.backgroundColor = '#19AEC6'; }
    if ( element["Count"] < element["Red threshold"] ){ row.style.backgroundColor = '#F48270'; }
    else if ( element["Count"] < element["Amber threshold"] ){ row.style.backgroundColor = '#FFD062'; }


    let cell = row.insertCell();
    let btn = document.createElement("div");
    let icon = document.createElement("i");
    icon.className = "fe-edit";
    btn.appendChild(icon);
    btn.addEventListener("click", function() {
      openModal(element)
    });
    cell.appendChild(btn);
  }
}

function getRequest(){
  console.log('send a request');
  Http.open("GET", url + qs + qsat + qsas + qsts);
  Http.send();
}
Http.open("GET", url + qs);
Http.send();

Http.onreadystatechange = (e) => {
  if(Http.readyState === XMLHttpRequest.DONE) {
    response = JSON.parse(Http.responseText);
    console.log(response);

    if (response.length > 0){
      let table = document.getElementById("table-stock-management");
      let data = Object.keys(response[0]);
      if (initFlag){

        generateTableHead(table, data);
        generateTable(table, response);
        initFlag = false;
      } else {
        //clear table
        clearTable(table);
        generateTable(table, response);
      }
    } else {
      clearTable();
    }

  }
}

const Http_item_type = new XMLHttpRequest();
var type_url = "http://192.168.0.52/ui/items/types"
Http_item_type.open("GET", type_url);
Http_item_type.send();


Http_item_type.onreadystatechange = (e) => {
  if(Http_item_type.readyState === XMLHttpRequest.DONE) {
    response = JSON.parse(Http_item_type.responseText);
    console.log(response);
    var select2Data = []
    for (itemType in response){
      var obj = {
        id:response[itemType]['id'],
        text:response[itemType]['type']
      }
      select2Data.push(obj);
    }
    $('#item-select').select2({
      dropdownParent: $("#signup-modal"),
      data:select2Data
    });
  }
}
