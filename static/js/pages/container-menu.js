const urlSearchParams = new URLSearchParams(window.location.search);
const params = Object.fromEntries(urlSearchParams.entries());
var id = params['id'];

document.getElementById("condition-monitoring").onclick = function () {
        location.href = "container-condition-monitoring.html?id=" + id;
    };
document.getElementById("alarms").onclick = function () {
        location.href = "container-alarms.html?id=" + id;
    };
document.getElementById("access-history").onclick = function () {
        location.href = "container-access-history.html?id=" + id;
    };
document.getElementById("stock-management").onclick = function () {
        location.href = "container-stock-management.html?id=" + id;
    };
document.getElementById("thresholds-settings").onclick = function () {
        location.href = "container-thresholds-settings.html?id=" + id;
    };
document.getElementById("orders").onclick = function () {
        location.href = "container-orders.html?id=" + id;
    };
