const urlSearchParams = new URLSearchParams(window.location.search);
const params = Object.fromEntries(urlSearchParams.entries());
var id = params['id'];
const Http = new XMLHttpRequest();
const url='http://192.168.0.52/ui/container/alarms';
var qs = "?id="+id;
var qsat = "";
var qsas = "";
var qsts = "";
var initFlag = true;

var tableIDs = [];

document.getElementById("container-menu-bc").onclick = function () {
        location.href = "container-menu.html?id=" + id;
    };

function clearTable(){
    var table = $('#table-alarms').DataTable();
    table.clear().draw();
}
document.getElementById("deactivate").onclick = function () {
        //get all ids of alarms
        $.ajax({
          url: "/get-session",
          type: 'GET',
          success: function(res) {
              console.log(res);
              var alarms = tableIDs;
              var user = res['id'];
              var timestamp = new Date().toISOString();
              var setAlarm = false;
              var data = {
                alarms: alarms,
                user: user,
                ts: timestamp,
                setAlarm: setAlarm
              }
              $.ajax({
                type: "POST",
                url: url + "/update/all",
                data: JSON.stringify(data),
                contentType: 'application/json; charset=utf-8',
                success: function(res) {
                  getRequest();
                }
              });
          }
        });
    };
function clearAlarmByID(id){
  // get session from Flask
  $.ajax({
    url: "/get-session",
    type: 'GET',
    success: function(res) {
        console.log(res);
        var alarm = id
        var user = res['id']
        var timestamp = new Date().toISOString()
        var setAlarm = false;
        var us = "?setAlarm="+setAlarm+"&ts="+timestamp+"&user="+user+"&alarm="+alarm
        $.ajax({
          url: url + "/update" + us,
          type: 'GET',
          success: function(res) {
              getRequest();
          }
        });
    }
  });
}

function status_btn_gen(cell, status, id) {
    let clearButton = document.createElement("div");
    clearButton.innerHTML = status
    if (status == "Active") {
        clearButton.className = "bg-success rounded-pill text-white text-center";
        clearButton.addEventListener("click", function () {
            clearAlarmByID(id);
          });
    }else {
        clearButton.className = "bg-danger rounded-pill text-white text-center";
    }
    cell.appendChild(clearButton);
}

function generateTable(data) {
    var data_table = $('#table-alarms').DataTable();
    let counter = 0;
    for (let element of data) {
        console.log(counter);
        tableIDs.push(element["Id"]);
        data_table.row.add([
            element["Alarm"],
            element["Description"],
            moment(element["Timestamp"]).format('DD/MM/YYYY, HH:mm:ss'),
            ""
        ]);
        var cell = data_table.cell(counter,3).node();
        status_btn_gen(cell, element["Status"],element["Id"])
        counter ++;
    }
    data_table.draw();
}
function tableInit() {
  $.fn.dataTable.moment('DD/MM/YYYY, HH:mm:ss');
  $("#table-alarms").DataTable({
    stateSave: false,
    order: [[2,'desc']],
    columnDefs: [
        { width: "65%", "targets": 1 }
    ],
    language: {
      paginate: {
        previous: "<i class='mdi mdi-chevron-left'>",
        next: "<i class='mdi mdi-chevron-right'>",
      },
    },
    drawCallback: function () {
      $(".dataTables_paginate > .pagination").addClass("pagination-rounded");
    },
  });
}

function getRequest(){
    console.log('send a request');
    Http.open("GET", url + qs + qsat + qsas + qsts);
    Http.send();
}

Http.onreadystatechange = (e) => {
  if (Http.readyState === XMLHttpRequest.DONE) {
    response = JSON.parse(Http.responseText);
    console.log(response);

    if (response.length > 0) {
      let data = Object.keys(response[0]);
      if (initFlag) {
        tableInit();
        generateTable(response);
        initFlag = false;
      } else {
        //clear table
        clearTable();
        generateTable(response);
      }
    } else {
        clearTable();
    }
  }
};

$('#alarm-type').on('change', function (e) {
  var selectData = $('#alarm-type').select2('data');
  // for selected data run a new get
  if (selectData.length > 0) {
    dataString = "(";
    for (data in selectData){
      dataString = dataString + "'" + selectData[data]['text'] + "',"
    }
    dataString = dataString.slice(0, -1);
    dataString = dataString + ")"
    console.log(dataString);
    qsat = '&at=' + dataString
  } else {
    qsat = ""
  }
  getRequest()
});
$('#alarm-state').on('change', function (e) {
  var selectData = $('#alarm-state').select2('data');
  if (selectData.length > 0) {
    dataString = "(";
    for (data in selectData){
      dataString = dataString + "" + selectData[data]['id'] + ","
    }
    dataString = dataString.slice(0, -1);
    dataString = dataString + ")"
    console.log(dataString);
    qsas = '&as=' + dataString
  } else {
    qsas = ""
  }
  getRequest()

});


!function($) {
    "use strict";
    var FormAdvanced = function() {};
    //initializing tooltip
    FormAdvanced.prototype.initSelect2 = function() {
        // Select2
        $('#alarm-type').select2();
        $('#alarm-state').select2();
    },
    //initilizing
    FormAdvanced.prototype.init = function() {
        var $this = this;
        this.initSelect2()
        $('.input-daterange-timepicker').daterangepicker({
          timePicker: true,
          locale: {format: 'DD/MM/YYYY hh:mm A'},
          timePickerIncrement: 30,
          timePicker12Hour: true,
          timePickerSeconds: false,
          buttonClasses: ['btn', 'btn-sm'],
          applyClass: 'btn-secondary',
          cancelClass: 'btn-primary',
          startDate: moment().subtract(1, 'days'),
          endDate: moment()
        },function (start, end) {
            console.log(start.toISOString(), end.toISOString());
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            qsts = "&t1=" + start.toISOString() + "&t2=" + end.toISOString()
            // Http.open("GET", url+qs);
            // Http.send();
            getRequest()

        });
    },
    $.FormAdvanced = new FormAdvanced, $.FormAdvanced.Constructor = FormAdvanced

}(window.jQuery),
    //initializing main application module
    function ($) {
        "use strict";
        $.FormAdvanced.init();
    }(window.jQuery);
$('#alarm-state').select2().val("TRUE").trigger('change');
getRequest();