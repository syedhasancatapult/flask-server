const urlSearchParams = new URLSearchParams(window.location.search);
const params = Object.fromEntries(urlSearchParams.entries());
var id = params['id'];
const Http = new XMLHttpRequest();
const url='http://192.168.0.52/ui/users';
var qs = "?im=0";
var initFlag = true;
var selectedRow = 0;

getRequest();

$('#item-type-select-box').select2();

$('#signup-modal').on('hidden.bs.modal', function () {
    selectedRow = 0;
    // inject itemtype into select2
    document.getElementById("input-name").value = "";
    document.getElementById("input-email").value = "";
    document.getElementById("input-pass").value = "";
    document.getElementById("input-pass2").value = "";
    document.getElementById("input-ac").value = "";
    $('#role-select').val(null).trigger('change');
});

document.getElementById("container-menu-bc").onclick = function () {
        location.href = "admin.html";
};

document.getElementById("btn-item-update").onclick = function () {
    // get item type
    var send = false;
    var pass = document.getElementById("input-pass").value;
    var pass2 = document.getElementById("input-pass2").value;
    if (selectedRow == 0){
      if (pass == ""){
        // password needed
        alert("Please enter a password");
      }
      else {
        if(pass!=pass2){
          // passwords don't match
          alert("Passwords do not match");
        }
        else {
          var data = {
            id: selectedRow,
            name: document.getElementById("input-name").value,
            email: document.getElementById("input-email").value,
            pass: document.getElementById("input-pass").value,
            accesscard: document.getElementById("input-ac").value,
            role: $('#role-select').val()
          }
          send = true;
        }
      }
    }else{
      if (pass != ""){
        if (pass == pass2){

          var data = {
            id: selectedRow,
            name: document.getElementById("input-name").value,
            email: document.getElementById("input-email").value,
            pass: document.getElementById("input-pass").value,
            accesscard: document.getElementById("input-ac").value,
            role: $('#role-select').select2('data')[0]['id']
          }
          send = true;

        }else{
          // passwords don't match
          alert("Passwords do not match");
        }
      }
      else{
        // send without password
        var data = {
          id: selectedRow,
          name: document.getElementById("input-name").value,
          email: document.getElementById("input-email").value,
          accesscard: document.getElementById("input-ac").value,
          role: $('#role-select').val()
        }
        send = true;
      }
    }
    if (send){
      $.ajax({
        type: "POST",
        url: 'http://192.168.0.52/ui/users/add',
        data: JSON.stringify(data),
        contentType: 'application/json; charset=utf-8',
        success: function(res) {
          getRequest();
          $('#item-select').val(null).trigger('change');
          $("#expiry-date-picker").val('');
          $('#device-select').val(null).trigger('change');
          selectedRow = 0;
          $('#signup-modal').modal('hide');
        }
      });
    }



};

function clearTable(table){
  $("#table-body").remove();
}
const Http_item_type = new XMLHttpRequest();
var type_url = "http://192.168.0.52/ui/users/roles"
Http_item_type.open("GET", type_url);
Http_item_type.send();


Http_item_type.onreadystatechange = (e) => {
  if(Http_item_type.readyState === XMLHttpRequest.DONE) {
    response = JSON.parse(Http_item_type.responseText);
    console.log(response)
    var select2Data = []
    for (roles in response){
      var obj = {
        id:response[roles]['id'],
        text:response[roles]['type']
      }
      select2Data.push(obj);
    }
    $('#role-select').select2({
      dropdownParent: $("#signup-modal"),
      data:select2Data
    });
  }
}

function updateUserState(id, state){
  data = {
    id: id,
    state: state
  }
  $.ajax({
    type: "POST",
    url: 'http://192.168.0.52/ui/users/update',
    data: JSON.stringify(data),
    contentType: 'application/json; charset=utf-8',
    success: function(res) {
      getRequest();
    }
  });
}

function getOptId(text) {
  let id = '';
  $('#item-select').find('*').filter(function() {
    if ($(this).text() === text) {
      id = $(this).val();
    }
  });
  return id;
}
function openModal(item) {
  selectedRow = item['Id']

  // inject itemtype into select2
  document.getElementById("input-name").value = item['Name'];
  document.getElementById("input-email").value = item['Email'];
  document.getElementById("input-pass").value = "";
  document.getElementById("input-pass2").value = "";
  document.getElementById("input-ac").value = item['Accesscard'];
  $('#role-select').val(getOptId(item['Role'])).change(); // Select the option with a value of '1'

  $('#signup-modal').modal('show');

}

function generateTableHead(table, data) {
  let thead = table.createTHead();
  let row = thead.insertRow();
  for (let key of data) {
    let th = document.createElement("th");
    let text = document.createTextNode(key);
    th.appendChild(text);
    row.appendChild(th);
  }
  let th = document.createElement("th");
  let text = document.createTextNode("Edit");
  th.appendChild(text);
  row.appendChild(th);
}
function generateTable(table, data) {
  let tbody = table.createTBody();
  tbody.setAttribute("id", "table-body");
  for (let element of data) {
    let row = tbody.insertRow();

    for (key in element) {
      if (key == "Active"){
        let cell = row.insertCell();
        let activeSwitch = document.createElement('input');
        activeSwitch.className = "js-switch"
        activeSwitch.type = "checkbox"
        activeSwitch.setAttribute("id", "switch-" + element['Id']);

        if (element[key]){
          activeSwitch.setAttribute("checked", "");
        }
        cell.appendChild(activeSwitch);


      }
      else{
        let cell = row.insertCell();
        let text = document.createTextNode(element[key]);
        cell.appendChild(text);
      }
    }
    let cell = row.insertCell();
    let btn = document.createElement("div");
    let icon = document.createElement("i");
    icon.className = "fe-edit";
    btn.appendChild(icon);
    btn.addEventListener("click", function() {
      openModal(element)
    });
    cell.appendChild(btn);

  }
  $('.js-switch').each(function (idx, obj) {
            new Switchery($(this)[0], $(this).data());

            $(this)[0].onchange = function() {
              var state = $(this)[0].checked;
              var id = parseInt($(this)[0].getAttribute('id').split('-')[1]);
              updateUserState(id, state);
            };
        });
}

function getRequest(){
  console.log('send a request');
  Http.open("GET", url + qs);
  Http.send();
}


Http.onreadystatechange = (e) => {
  if(Http.readyState === XMLHttpRequest.DONE) {
    response = JSON.parse(Http.responseText);
    console.log(response);

    if (response.length > 0){
      let table = document.getElementById("table-alarms");
      let data = Object.keys(response[0]);
      if (initFlag){

        generateTableHead(table, data);
        generateTable(table, response);
        initFlag = false;
      } else {
        //clear table
        clearTable(table);
        generateTable(table, response);
      }
    } else {
      clearTable();
    }

  }
}
