const urlSearchParams = new URLSearchParams(window.location.search);
const params = Object.fromEntries(urlSearchParams.entries());
var id = params['id'];
const Http = new XMLHttpRequest();
const url='http://192.168.0.52/ui/container-management';
var initFlag = true;
var selectedRow = 0;

getRequest();

$('#item-type-select-box').select2();

$('#signup-modal').on('hidden.bs.modal', function () {
    selectedRow = 0;
    document.getElementById("input-cd").value = "";
    document.getElementById("input-cs").value = "";
    document.getElementById("input-cc").value = "";
});

document.getElementById("container-menu-bc").onclick = function () {
        location.href = "admin.html";
};

document.getElementById("btn-item-update").onclick = function () {
    // get item type
    var cd = document.getElementById("input-cd").value
    var cs = document.getElementById("input-cs").value
    var cc = document.getElementById("input-cc").value
    // send to endpoint
    var data = {
      id: selectedRow,
      cd: cd,
      cs: cs,
      cc: cc
    }
    $.ajax({
      type: "POST",
      url: url + '/add',
      data: JSON.stringify(data),
      contentType: 'application/json; charset=utf-8',
      success: function(res) {
        getRequest();
        document.getElementById("input-cd").value = "";
        document.getElementById("input-cs").value = "";
        document.getElementById("input-cc").value = "";
        selectedRow = 0;
        $('#signup-modal').modal('hide');
      }
    });
};

function clearTable(table){
  $("#table-body").remove();
}

function openModal(item) {
  selectedRow = item['Id']
  document.getElementById("input-cd").value = item['Description'];
  document.getElementById("input-cs").value = item['Size'];
  document.getElementById("input-cc").value = item['Capacity'];

  $('#signup-modal').modal('show');

}

function generateTableHead(table, data) {
  let thead = table.createTHead();
  let row = thead.insertRow();
  for (let key of data) {
    let th = document.createElement("th");
    let text = document.createTextNode(key);
    th.appendChild(text);
    row.appendChild(th);
  }
  let th = document.createElement("th");
  let text = document.createTextNode("Edit");
  th.appendChild(text);
  row.appendChild(th);
}
function generateTable(table, data) {
  let tbody = table.createTBody();
  tbody.setAttribute("id", "table-body");
  for (let element of data) {
    let row = tbody.insertRow();

    for (key in element) {
      let cell = row.insertCell();
      let text = document.createTextNode(element[key]);
      cell.appendChild(text);
    }
    let cell = row.insertCell();
    let btn = document.createElement("div");
    let icon = document.createElement("i");
    icon.className = "fe-edit";
    btn.appendChild(icon);
    btn.addEventListener("click", function() {
      openModal(element)
    });
    cell.appendChild(btn);

  }
}

function getRequest(){
  console.log('send a request');
  Http.open("GET", url);
  Http.send();
}


Http.onreadystatechange = (e) => {
  if(Http.readyState === XMLHttpRequest.DONE) {
    response = JSON.parse(Http.responseText);
    console.log(response);

    if (response.length > 0){
      let table = document.getElementById("table-alarms");
      let data = Object.keys(response[0]);
      if (initFlag){

        generateTableHead(table, data);
        generateTable(table, response);
        initFlag = false;
      } else {
        //clear table
        clearTable(table);
        generateTable(table, response);
      }
    } else {
      clearTable();
    }

  }
}
