#!/bin/bash
source /root/.bashrc

adddate() {
    while IFS= read -r line; do
        printf '%s %s\n' "$(date)" "$line";
    done
}

conda run --no-capture -n flask_server python /home/das/flask-server/bluegate.py | adddate &>> /root/logs/bluetoothLog.txt