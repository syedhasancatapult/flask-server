#!/bin/bash

adddate() {
    while IFS= read -r line; do
        printf '%s %s\n' "$(date)" "$line";
    done
}

sed -i "/^DEVICES=.*/c\DEVICES=\"/dev/ttyUSB1\"" /etc/default/gpsd

echo AT+CGPS=0,1 | socat - /dev/ttyUSB3,crnl | adddate &>> /root/logs/gpsLog.txt
wait 60
echo AT+CGPS=1,1 | socat - /dev/ttyUSB3,crnl | adddate &>> /root/logs/gpsLog.txt