systemctl restart gpsd.socket gpsd.service

sed -i "/^DEVICES=.*/c\DEVICES=\"/dev/ttyUSB1\"" /etc/default/gpsd >> /root/logs/gpsLog.txt

echo AT+CGPS=0,1 | socat - /dev/ttyUSB3,crnl >> /root/logs/gpsLog.txt

echo AT+CGPS=1,1 | socat - /dev/ttyUSB3,crnl >> /root/logs/gpsLog.txt

cgps &>> /root/logs/gpsLog.txt &

sudo python3 /home/das/bluegate.py 2>&1 | tee -a /root/logs/bluetoothLog.txt &

conda run --no-capture -n flask_server python /home/das/flask-server/run.py &>> /root/logs/flaskServerLog.txt &
