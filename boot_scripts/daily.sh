#!/bin/bash

source /root/.bashrc

adddate() {
    while IFS= read -r line; do
        printf '%s %s\n' "$(date)" "$line";
    done
}

python /home/das/flask-server/daily_alarms.py | adddate &>> /root/logs/dailyTaskLog.txt
