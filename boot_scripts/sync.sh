#!/bin/bash

source /root/.bashrc

adddate() {
    while IFS= read -r line; do
        printf '%s %s\n' "$(date)" "$line";
    done
}

python /home/das/flask-server/syncdb.py | adddate &>> /root/logs/syncLog.txt
