#!/bin/bash

apt-get update
apt-get install -y lsb-release
apt-get clean all
sh -c 'echo "deb https://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
apt-get install gnupg
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
apt-get update
apt-get -y install postgresql-12
apt-get -y install postgresql-12-cron

echo "shared_preload_libraries = 'pg_cron'" >> /etc/postgres/main/12/postgresql.conf
echo "cron.database_name = 'postgres'" >> /etc/postgres/main/12/postgresql.conf

#restart postgres