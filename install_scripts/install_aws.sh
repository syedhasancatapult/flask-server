#!/bin/bash

source /root/.bashrc

python -m pip install awsiotsdk

# Prerequisites
sudo apt install cmake -y
sudo apt install openssl -y

# Building
git clone https://github.com/awslabs/aws-iot-device-client
cd aws-iot-device-client
mkdir build
cd build
cmake ../
cmake --build . --target aws-iot-device-client

# Setup
cd ../
./setup.sh # At this point you'll need to respond to prompts for information, including paths to your thing certs

# Run the AWS IoT Device Client
./aws-iot-device-client # This command runs the executable

