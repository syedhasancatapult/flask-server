#!/bin/bash
# Copy service and timer files to /etc/systemd/system/

mkdir ~/logs
echo. 2>~/logs/flaskServerLog.txt
echo. 2>~/logs/gpsLog.txt
echo. 2>~/logs/service.txt
echo. 2>~/logs/syncLog.txt
echo. 2>~/logs/bluetoothLog.txt

cp /home/das/flask-server/boot_scripts/dasble.service /etc/systemd/system/
cp /home/das/flask-server/boot_scripts/dasflask.service /etc/systemd/system/
cp /home/das/flask-server/boot_scripts/dasgps.service /etc/systemd/system/
cp /home/das/flask-server/boot_scripts/dasservice.service /etc/systemd/system/
cp /home/das/flask-server/boot_scripts/dassync.service /etc/systemd/system/
cp /home/das/flask-server/boot_scripts/dassync.timer /etc/systemd/system/

systemctl enable dasgps.service
systemctl enable dasflask.service
systemctl enable dasble.service
systemctl enable dassync.service
systemctl enable dassync.timer
systemctl enable dasservice.service

