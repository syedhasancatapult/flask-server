#!/bin/bash
source ~/.bashrc
# Create environment
conda init
conda env create --file /home/das/flask-server/environment.yml
echo 'conda activate flask_server' >> ~/.bashrc