import psycopg2
from starlink import StarlinkPowerCycle as SPC
from datetime import datetime
import time
import sys, os
from os.path import join, dirname
from dotenv import load_dotenv
import requests
from requests.exceptions import Timeout, RequestException, ConnectionError

dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)

def get(sql):
    result = 0
    try:
        conn = psycopg2.connect(
            host="localhost",
            port="5432",
            dbname="das",
            user="postgres",
            password="password"
        )

        cur = conn.cursor()
        sql = sql + ';'
        cur.execute(sql)
        result = cur.fetchall()
    finally:
        try:
            conn.close()
        except:
            pass
    return result

def satellite_comms(power:bool):
    spc = SPC()
    if power:
        spc.control_relay_1_on()
        if not spc.online:
            sys.exit()
        else:
            print("Satellite connection established")
    else:
        spc.control_relay_1_off()
        print("Satellite connection powered down")

def connect_to_internet():
    url = os.environ.get("CLOUD_API_ENDPOINT") + "sync/"
    try:
        r = requests.get(url, timeout=30)
    except (Timeout, ConnectionError, RequestException) as e:
        print("Requests exception raised")
        print(e)
        satellite_comms(True)

def get_start_end_time() -> tuple:
    sql = """SELECT service_start_ts, service_end_ts
    FROM containers
    WHERE container_id = {}
    """.format(int(os.environ.get("CONTAINER_ID")))

    return get(sql)[0]

def restart_tunnel():
    if int(os.environ.get("CONTAINER_ID")) == 1:
        os.popen("systemctl restart ngrok.service")
    else:
        os.popen("systemctl restart aws-iot-device-client.service")

# script starts here ---------------------------------
start_ts, end_ts = get_start_end_time()

current_ts = datetime.now()

if start_ts < current_ts < end_ts:
    print("service in place")
    connect_to_internet()
    restart_tunnel()
    while datetime.now() < end_ts:
        print("in service", flush=True)
        start_ts, end_ts = get_start_end_time()
        time.sleep(10)
    print("service ending")

    satellite_comms(False)

# else:
#     print("no service in place")
# print("bye")
# script ends here -----------------------------------