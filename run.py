from flask import Flask, render_template, request, redirect, url_for, session
import logging
import secrets
import re
import requests
from DeviceClass.Helpers import door_mimic, condition_monitoring_event_management
from Database.Postgres import Postgres
from DeviceClass.ConditionMonitoring import ConditionMonitoring
from DeviceClass.AssetTracking import AssetTracking
import DeviceClass.LambdaLogic as LambdaLogic
from DeviceClass.Helpers import access_card_security_check
from datetime import datetime
import json
import os
import traceback
from dotenv import load_dotenv

log = logging.getLogger('werkzeug')
log.setLevel(logging.ERROR)


app = Flask(__name__)

app.secret_key = secrets.token_hex(16)


# ------------------ Containers ------------------------------------
@app.route("/container-map.html")
def container_map():
    return render_template('container-map.html')


@app.route("/container-menu.html")
def container_menu():
    container_id = request.form.get('id')
    return render_template('container-menu.html', query=container_id)


@app.route("/container-condition-monitoring.html")
def container_condition_monitoring():
    container_id = request.form.get('id')
    return render_template('container-condition-monitoring.html', query=container_id)


@app.route("/container-alarms.html")
def container_alarms():
    container_id = request.form.get('id')
    return render_template('container-alarms.html', query=container_id)


@app.route("/container-access-history.html")
def container_access_history():
    container_id = request.form.get('id')
    return render_template('container-access-history.html', query=container_id)


@app.route("/container-stock-management.html")
def container_stock_management():
    container_id = request.form.get('id')
    return render_template('container-stock-management.html', query=container_id)


@app.route("/container-thresholds-settings.html")
def container_thresholds_settings():
    container_id = request.form.get('id')
    return render_template('container-thresholds-settings.html', query=container_id)


@app.route("/container-orders.html")
def container_orders():
    container_id = request.form.get('id')
    return render_template('container-orders.html', query=container_id)


# ------------------ Items ------------------------------------
@app.route("/items.html")
def items_menu():
    return render_template('items.html')


@app.route("/item-type-search.html")
def item_type_search():
    return render_template('item-type-search.html')


@app.route("/container-search.html")
def container_search():
    return render_template('container-search.html')


@app.route("/item-management.html")
def item_management():
    return render_template('item-management.html')


# ------------------ Admin ------------------------------------
@app.route("/admin.html")
def admin():
    return render_template('admin.html')


@app.route("/user-management.html")
def user_management():
    return render_template('user-management.html')


@app.route("/container-management.html")
def container_management():
    return render_template('container-management.html')


@app.route("/item-type-management.html")
def item_type_management():
    return render_template('item-type-management.html')


# ------------------ Authentication ------------------------------------
@app.route("/")
def index():
    # Check if cookie exists
    app.logger.info('homeroute')
    logged_in = 'user' in session
    if logged_in:
        return redirect(url_for('container_map'))
    # Check if card has been swipped
    db = Postgres()
    sql = """SELECT * FROM users where user_id = (
            SELECT 
                         CASE
                              WHEN access_type = 'true' THEN user_id
                              ELSE 0
                         END as test
            FROM container_access
            order by timestamp desc
            limit 1
        )
        """
    user = db.get(sql)
    if len(db.get(sql)) == 0:
        temp_bool = False
    else:
        user_id, name, username, password, accesscard, role_id, active = user[0]
        obj = {
            "id": user_id,
            "name": name,
            "username": username,
            "accesscard": accesscard,
            "role_id": role_id
        }
        session['user'] = obj
        temp_bool = True
    if temp_bool:
        # drop cookie
        return redirect(url_for('container_map'))
    else:
        return redirect(url_for('login_select'))


@app.route("/login-select")
def login_select():
    return render_template('login-select.html')


@app.route("/auth.html")
def auth():
    return render_template('auth.html')


@app.route("/log-out.html")
def logout():
    # delete cookie
    dotenv_path = '/home/das/flask-server/.env'
    load_dotenv(dotenv_path)
    door_mac = os.environ.get("DOOR")
    if door_mac == "NODOOR":
        door_mimic(door_mac, False)
    # session.pop('user', default=None)
    session.clear()
    app.secret_key = secrets.token_hex(16)
    return redirect(url_for('index'))


@app.route('/api/login', methods=['POST'])
def app_login():
    if request.method == 'POST':
        request_data = request.get_json()
        app.logger.info(request_data)
        email = request_data['username']
        passw = request_data['password']
        url = 'http://localhost/ui/login'
        obj = {
            'username': email,
            'password': passw
        }

        r = requests.post(url, json=obj)
        app.logger.info(r.json())
        if r.status_code == 200:
            # log this user in the access table
            dotenv_path = '/home/das/flask-server/.env'
            load_dotenv(dotenv_path)
            session['user'] = r.json()

            door_mac = os.environ.get("DOOR")
            if door_mac == "NODOOR":
                door_mimic(door_mac, True)

            data = {
                "addr": "cardreader",
                "timestamp": datetime.now().strftime("%m/%d/%Y, %H:%M:%S"),
                "parsed_data": {"UID": session["user"]["id"]}
            }
            return condition_monitoring_event_management(data), 200
        if r.status_code == 403:
            return '403'
        if r.status_code == 404:
            return '404'


@app.route('/api/condition-monitoring', methods=['POST'])
def api_condition_monitoring():
    if request.method == 'POST':
        request_data = request.json
        app.logger.info(request_data)


        dotenv_path = '/home/das/flask-server/.env'
        load_dotenv(dotenv_path)
        try:
            cm_data = {
                "addr": request_data["addr"].upper(),
                "timestamp": request_data['timestamp'],
                "parsed_data": request_data['parsed_data']
            }
            cm = ConditionMonitoring(cm_data)
            device_type = cm.set_device_type()
            app.logger.info(device_type)
            event = cm.send()
            if event == "log_out":
                session.clear()
                app.secret_key = secrets.token_hex(16)
        except Exception as e:
            app.logger.info("EXCEPTION")
            app.logger.info(e)
            if str(e) != "Device Type unknown":
                app.logger.error(traceback.format_exc())
                return str(e)
        except KeyError as ke:
            app.logger.info(str(ke))

        return '200'


@app.route('/api/access-card', methods=['POST'])
def api_card_reader():
    if request.method == 'POST':
        dotenv_path = '/home/das/flask-server/.env'
        load_dotenv(dotenv_path)
        door_mac = os.environ.get("DOOR")
        if door_mac == "NODOOR":
            door_mimic(door_mac, True)

        request_data = request.form.to_dict()
        app.logger.info(request_data['UID'])

        user_id = access_card_security_check(request_data["UID"])
        if user_id== 0:
            app.logger.info("unauthorised access")
            return "unauthorised access", 401

        data = {
            "addr": "cardreader",
            "timestamp": datetime.now().strftime("%m/%d/%Y, %H:%M:%S"),
            "parsed_data": {"UID": user_id}
        }
        return condition_monitoring_event_management(data), 200


@app.route('/api/asset-tracking', methods=['POST'])
def api_asset_tracking():
    if request.method == 'POST':
        request_data = request.json
        try:
            AssetTracking(request_data)
        except Exception as e:
            # app.logger.info(str(e))
            return "Device does not exist", 401
        return "Device has been registered", 200


@app.route('/api/shock-event', methods=['POST'])
def shock_event():
    if request.method == 'POST':
        request_data = request.json
        request_data["timestamp"] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        try:
            with open("/home/das/shock.txt", "a+") as f:
                f.write(str(request_data)+"\n")
        except Exception as e:
            # app.logger.info(str(e))
            return "Device does not exist", 401
        return "Device has been registered", 200


@app.route('/api/shutdown', methods=['GET'])
def api_shutdown():
    if request.method == 'GET':
        print('shutting down')
        stream = os.popen('sudo shutdown')
        output = stream.read()
        return output


@app.route('/api/reboot', methods=['GET'])
def api_reboot():
    if request.method == 'GET':
        print('rebooting')
        stream = os.popen('sudo shutdown -r 1')
        output = stream.read()
        return output


@app.route('/api/sync', methods=['GET'])
def api_sync():
    if request.method == 'GET':
        print('attempthing sync')
        stream = os.popen('systemctl start dassync.service')
        output = stream.read()
        return output


# ------------------ UI logic endpoints ------------------------------------
@app.route("/ui/<path:path>", methods=['GET', 'POST'])
def test_route(path):

    event = {
        "path": '/ui/' + path,
        "body": request.get_json() if request.method == 'POST' else json.dumps(request.args.to_dict()),
        "httpMethod": request.method,
        "queryStringParameters": request.args.to_dict()
    }
    hello = LambdaLogic.lambda_handler(event, "context")
    return hello['body'], hello['statusCode'], hello['headers']


# COOKIES AND REDIRECTS ---------------------------------------------
@app.route("/get-session")
def get_session():
    response = app.response_class(
        response=json.dumps(session['user']),
        status=200,
        mimetype='application/json'
    )
    return response


@app.before_request
def load_user():
    # does session exist
    path = request.path
    logged_in = 'user' in session

    # tapped_in =

    if logged_in:
        pass
    if path in ['/login-select', '/auth.html', '/', '/api/login', '/ui/login']:
        app.logger.info('no need to redirect')
    elif 'user' not in session and not re.match('/static/', request.path) and not re.match('/api/', request.path):
        app.logger.info(path)
        return redirect(url_for('login_select'))


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)
