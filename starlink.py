import socket
from enum import Enum
import sys
import time
import requests
from requests.exceptions import ConnectTimeout, ConnectionError


class Relay(Enum):
    ONE = 1
    TWO = 2


class Power(Enum):
    ON = 1
    OFF = 2


class Errors(Enum):
    NO_POWER = 1
    NO_NETWORK = 2
    NO_INTERNET = 3


class Timeouts(Enum):
    DEFAULT = 300
    WARMUP = 300
    CONNECTION = 300
    FAIL = 60
    BACKOFF = 120

class Retries(Enum):
    DEFAULT = 3

class StarlinkPowerCycle:
    def __init__(self, host: str = "192.168.0.105", port: int = 5001):
        """
        This Class is used to control the relay that switches the Starlink on and off
        :param host: ip address in string format for ip relay device
        :param port: port on ip relay device
        """
        self.relay_1_power_state = False
        self.relay_2_power_state = False
        self.HOST = host  # The server's hostname or IP address
        self.PORT = port  # The port used by the server
        self.online = False

    @staticmethod
    def das_sleep(time_in_seconds:int):
        print("going to sleep for for {} seconds".format(time_in_seconds))
        time.sleep(time_in_seconds)

    def control_relay_1_on(self):
        """
        This method will turn the power of the starlink on
        When powering up we will wait for a connection within 20 minutes attempting this three times
        :return: True when you have successfully turned on
        """
        print("powering on relay")
        attempts = 0

        while attempts < Retries.DEFAULT.value:
            self._relay_control(Relay.ONE, Power.ON)
            self.das_sleep(Timeouts.WARMUP.value + (Timeouts.BACKOFF.value * attempts))
            print("Attempting to connect...")
            try:
                x = requests.get('https://google.com', timeout=Timeouts.CONNECTION.value)
                break
            except (ConnectTimeout, ConnectionError) as e:
                self._relay_control(Relay.ONE, Power.OFF)
                print(e.response)
                print("Attempt number " + str(attempts) + " has failed")
                self.das_sleep(Timeouts.FAIL.value)
                attempts = attempts + 1
        if attempts < Retries.DEFAULT.value:
            self.online = True
        else:
            self._relay_control(Relay.ONE, Power.OFF)
            self.online = False

    def control_relay_1_off(self) -> bool:
        """
        This method will turn the power of the starlink off
        When powering down we will wait for any connections to end and then kill the power
        :return: True when
        """
        self._relay_control(Relay.ONE, Power.OFF)
        return True

    def _message_builder(self, relay: Relay, power: Power) -> bytes:
        if power == Power.ON:
            if relay == Relay.ONE:
                self.relay_1_power_state = True
                return b"on1"
            else:
                self.relay_2_power_state = True
                return b"on2"
        else:
            if relay == Relay.ONE:
                self.relay_1_power_state = False
                return b"off1"
            else:
                self.relay_2_power_state = False
                return b"off2"

    def _relay_control(self, relay: Relay, power: Power) -> str:
        my_bytes = self._message_builder(relay, power)
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.connect((self.HOST, self.PORT))
            s.sendall(my_bytes)
            data = s.recv(1024)
        return f"Received {data!r}"


def main():
    args = sys.argv
    starlink = StarlinkPowerCycle()
    for arg in args:
        if arg == 'on':
            starlink.control_relay_1_on()
            if starlink.online:
                print("You are connected")
            else:
                print("You failed to connect")

        elif arg == 'off':
            starlink.control_relay_1_off()
        else:
            print("Please provide an argument 'on' or 'off'")


if __name__ == "__main__":
    main()
