import os
from os.path import join, dirname
from dotenv import load_dotenv
from Database.Postgres import Postgres
from DeviceClass import Helpers
from DeviceClass.AlarmTypes import AlarmType


def run_stock_check():
    """
    Raise an alarm if:
    - The item type stock level goes below the defined red threshold level for this container
    """
    db_connection = Postgres()
    temp_table = """
    select temporary.raise_alarm, temporary.red_th, it.type
    from 
    (SELECT 
        CASE
            WHEN (select count(i.item_id) 
                    from item_container_history ich 
                    join items i on ich.item_id=i.item_id 
                    where i.type_id = cst.item_type_id
                    and ich.checked_in_out=true) < red_th
                THEN true
                ELSE false
            END AS raise_alarm, *
    FROM container_stock_thresholds as cst) as temporary, item_type it
    where temporary.item_type_id = it.type_id
    """
    results = db_connection.get(temp_table)
    for result in results:
        raise_alarm, threshold, item_type_name = result
        if raise_alarm:
            print("stock low on {}, attemping to raise alarm".format(item_type_name))
            # check if an active alarm already exists with this description
            container_id = int(os.environ.get("CONTAINER_ID"))
            description = """Stock levels in container {} are below the red threshold for item type {} of {} items
            """.format(container_id, item_type_name, threshold)
            alarm_id = Helpers.active_alarm_already_exist_by_description(description)
            if alarm_id > 0:
                Helpers.update_alarm_timestamp(alarm_id)
                pass
            else:Helpers.raise_alarm(
                alarm_type=AlarmType.StockLevels,
                description=description,
                container_id=container_id,
            )


def run_expiry_check():
    """
    Raise an alarm if:
    - An item has expired
    """
    db_connection = Postgres()
    temp_table = """
        SELECT h.item_id, it.type, i.expiry_date, i.alarm
        FROM item_container_history h, items i, item_type it
        WHERE h.item_id=i.item_id AND i.type_id = it.type_id AND h.checked_in_out='true' AND expiry_date <= now()
        """
    results = db_connection.get(temp_table)
    for result in results:
        item_id, item_type, exp_date, item_alarm_state = result
        if not item_alarm_state:
            container_id = int(os.environ.get("CONTAINER_ID"))
            description = """The Item {} {} has expired on {}
                        """.format(item_id, item_type, exp_date)
            Helpers.raise_alarm(
                alarm_type=AlarmType.ThresholdBreached,
                description=description,
                container_id=container_id,
                item_id=item_id
            )
#             todo put item into alarm state
            Helpers.set_item_alarm_state(item_id, True)


def main():
    """
    This script runs daily, it will raise alarms in the db when:
    - The item type stock level goes below the defined red threshold level for this container
    - An item has expired
    """
    dotenv_path = join(dirname(__file__), '.env')
    load_dotenv(dotenv_path)
    run_stock_check()
    run_expiry_check()


if __name__ == "__main__":
    main()
