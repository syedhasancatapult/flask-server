#! /usr/bin/env python3
import os
import struct
from os.path import join, dirname
from dotenv import load_dotenv
from my_thingy52 import Thingy52
from bluepy.btle import DefaultDelegate
import time
import traceback
import pprint

# -- Load system env -----------------------------------------------------

dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)

# -- Default configuration -----------------------------------------------------
class ThingyDelegate(DefaultDelegate):
    def __init__(self, environment_service, motion_service):
        self.environment_service = environment_service
        self.motion_service = motion_service
        self.received_values = {}

    def handleNotification(self, hnd, data):
        if (hnd == self.environment_service.temperature_char.getHandle()):
            # https://github.com/NordicSemiconductor/Nordic-Thingy52-FW/blob/126120108879d5bf5d202c9d5cab65e4e9041f58/source/modules/m_environment.c

            intg, frac = struct.unpack("<bB", data)
            temp_val = intg + frac/100

            # print(f'Notification: Temp received:  {temp_val} degCelcius')
            self.received_values["temperature"] = temp_val

        elif (hnd == self.environment_service.humidity_char.getHandle()):
            hum_val = struct.unpack("<b", data)[0]
            # print(f'Notification: Humidity received: {hum_val} %')
            self.received_values["humidity"] = hum_val

        elif (hnd == self.motion_service.rawdata_char.getHandle()):
            motion_raw = struct.unpack("<hhhhhhhhh", data)
            a_x, a_y, a_z, g_x, g_y, g_z, c_x, c_y, c_z = motion_raw
            motion_object = {
                "Accelerometer x": a_x,
                "Accelerometer y": a_y,
                "Accelerometer z": a_z,
                "Gyroscope x": g_x,
                "Gyroscope y": g_y,
                "Gyroscope z": g_z,
                "Compass x": c_x,
                "Compass y": c_y,
                "Compass z": c_z
            }
            if "motion_raw" in self.received_values:
                self.received_values["motion_raw"].append(motion_object)
            else:
                self.received_values["motion_raw"] = [motion_object]

    def hasReceivedAll(self):
        return len(self.received_values) >= 3 and len(self.received_values["motion_raw"]) >= 5

thingy_addr = os.environ.get("THINGY_MAC")

def talkToThingy(addr):
    print(f'Connecting to {addr}...')
    t = Thingy52(addr)
    print('Connected')

    t.sound.enable()
    t.sound.configure(speaker_mode=0x03)  # 0x03 means sample mode, ref FW doc
    t.sound.play_speaker_sample(1)

    delegate = ThingyDelegate(t.environment, t.motion)

    t.setDelegate(delegate)
    t.environment.enable()
    t.motion.enable()

    t.environment.configure(temp_int=1000, humid_int=1000, color_int=1000)

    while True:
        # print('entering while loop')
        t.environment.set_temperature_notification(True)
        t.environment.set_humidity_notification(True)
        t.motion.set_rawdata_notification(True)

        while not delegate.hasReceivedAll():
            pass
        if delegate.hasReceivedAll():
            # print('all packets received')
            pprint.pprint(delegate.received_values)
            # print("clearing received values")
            delegate.received_values = {}




try:
    thingy_addr = thingy_addr.lower()
    while True:
        try:
            talkToThingy(thingy_addr)
        except Exception as e:
            print("Exception while connecting to Thingy")
            print(traceback.format_exc())
        # send_condition_monitoring_payload(thingy_addr, gps_fix, thingy_payload, monitoring_url, HTTP_TIMEOUT)
        time.sleep(5)


except KeyboardInterrupt:

    print()

print("Stopping the BLE gateway service")
