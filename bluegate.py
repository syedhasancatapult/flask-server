#! /usr/bin/env python3
import os
from os.path import join, dirname
from dotenv import load_dotenv
import argparse
from scanner import *
from thingy import *
from DeviceClass.GPS import GPS
# -- Load system env -----------------------------------------------------

dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)

# -- Default configuration -----------------------------------------------------

HCI = 0 # Bluetooth interface number
THINGY_POLL_INTERVAL = 300 # seconds

HTTP_TIMEOUT = 30 # seconds
SCAN_DURATION = 10 # seconds


# ------------------------------------------------------------------------------

asset_url = "http://127.0.0.1/api/asset-tracking"
monitoring_url = "http://127.0.0.1/api/condition-monitoring"

door_set = {
     os.environ.get("DOOR")
     }

thingy_addr = os.environ.get("THINGY_MAC")
print(thingy_addr)

debug_set = {
    "57:40:32:c3:5e:ca",
    "04:69:f8:8d:39:b7"
     }

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--hci', action='store', type=int, default=HCI,
                    help='BT interface number for scanning')
parser.add_argument('-v', '--verbose', action='store_true',
                    help='Increase output verbosity')
args = parser.parse_args()

if args.verbose:
    logging.basicConfig(level=logging.DEBUG)
else:
    logging.basicConfig(level=logging.INFO)

disp = DataDispatcher(asset_url)
if os.environ.get("DOOR") != "NODOOR":
    disp.addDataHandler(door_set, monitoring_url, parseDoor)
# disp.addDataHandler([thingy_addr], monitoring_url, None)

logging.info("Starting the BLE gateway service")


try:
    thingy_addr = thingy_addr.upper()
    last_time_thingy = time.time()
    failure_cnt = 0
    while True:
        discovered_devices = []
        try:
            s = Scanner(disp, args.hci, HTTP_TIMEOUT)
            discovered_devices = s.scan(scan_duration = SCAN_DURATION)
            failure_cnt = 0
        except Exception as e:
            logging.exception("Exception while scanning")
            failure_cnt += 1

        if time.time() - last_time_thingy >= THINGY_POLL_INTERVAL:
            # time to talk to the Thingy
            thingy_payload = {}
            gps_fix = GPS().get_latest_fix()
            if thingy_addr in (x.addr.upper() for x in discovered_devices):
                try:
                    thingy_payload = talkToThingy(thingy_addr)
                    failure_cnt = 0
                    last_time_thingy = time.time()
                except Exception as e:
                    logging.exception("Exception while connecting to Thingy")
                    failure_cnt += 1
            else:
                logging.info("Time to talk to Thingy but it is not discovered")
            send_condition_monitoring_payload(thingy_addr, gps_fix, thingy_payload, monitoring_url, HTTP_TIMEOUT)
        
        if failure_cnt > 10:
            time.sleep(1) # in case of repeated failures, go to sleepy mode

except KeyboardInterrupt:
    print()

logging.info("Stopping the BLE gateway service")
