from bluepy.btle import UUID, Peripheral, ADDR_TYPE_RANDOM, DefaultDelegate
import my_thingy52 as thingy52
import struct
import json
import time
import logging
import requests


class ThingyDelegate(DefaultDelegate):

    def __init__(self, environment_service):
        self.environment_service = environment_service
        self.received_values = {}
    
    def handleNotification(self, hnd, data):
        if (hnd == self.environment_service.temperature_char.getHandle()):
            # https://github.com/NordicSemiconductor/Nordic-Thingy52-FW/blob/126120108879d5bf5d202c9d5cab65e4e9041f58/source/modules/m_environment.c

            intg, frac = struct.unpack("<bb", data)
            temp_val = intg + frac/100

            logging.debug(f'Notification: Temp received:  {temp_val} degCelcius')
            self.received_values["temperature"] = temp_val

        elif (hnd == self.environment_service.humidity_char.getHandle()):
            hum_val = struct.unpack("<b", data)[0]
            logging.debug(f'Notification: Humidity received: {hum_val} %')
            self.received_values["humidity"] = hum_val

        elif (hnd == self.environment_service.color_char.getHandle()):
            red, green, blue, clear = struct.unpack("<HHHH", data)
            r_8, g_8, b_8 = self._convertColor(red, green, blue, clear)
            color_spec = {
                    "red":r_8,
                    "green":g_8,
                    "blue":b_8,
                    }
            logging.debug(f'Notification: Color: {color_spec}')
            self.received_values["color"] = color_spec

    def _convertColor(self, red, green, blue, alpha):
        total = max(red + green + blue, 1) # to avoid NaN when rgb values could be zeros
        r_ratio = red / total
        g_ratio = green / total
        b_ratio = blue / total

        clear_at_black = 300
        clear_at_white = 400
        clear_diff = clear_at_white - clear_at_black

        clear_normalized = (alpha - clear_at_black) / clear_diff
        if clear_normalized < 0: clear_normalized = 0

        r_8 = int(r_ratio * 255 * 3 * clear_normalized)
        if r_8 > 255: r_8 = 255

        g_8 = int(g_ratio * 255 * 3 * clear_normalized)
        if g_8 > 255: g_8 = 255

        b_8 = int(b_ratio * 255 * 3 * clear_normalized)
        if b_8 > 255: b_8 = 255

        return r_8, g_8, b_8


    def hasReceivedAll(self):
        return len(self.received_values) >= 3

def send_condition_monitoring_payload(addr, gps_payload, thingy_payload, url, http_timeout):
    if all(not bool(v) for v in [gps_payload, thingy_payload]):
        return None
    if gps_payload is None:
        parsed_data = thingy_payload
        pass
    elif thingy_payload is None:
        parsed_data = gps_payload
        pass
    else:
        parsed_data = {**thingy_payload, **gps_payload}

    payload = {
        "addr": addr,
        "timestamp": time.strftime("%Y-%m-%d %H:%M:%S"),
        "parsed_data": parsed_data
    }
    try:
        logging.info('posting message {}'.format(payload))
        return requests.post(
            url,
            json=payload,
            timeout=http_timeout)
    except Exception as e:
        logging.info('Exception')
        logging.error("Failed to connect to the server")
        logging.error(e)
        return None

def talkToThingy(addr):
    logging.info(f'Connecting to {addr}...')
    t = thingy52.Thingy52(addr)
    logging.info('Connected')

    delegate = ThingyDelegate(t.environment)
    t.setDelegate(delegate)
    t.environment.enable()
    t.battery.enable()
    #t.environment.configure(temp_int=1000, humid_int=1000, color_int=1000, color_sens_calib=[0,0,0])
    t.environment.configure(temp_int=1000, humid_int=1000, color_int=1000)
    t.environment.set_temperature_notification(True)
    t.environment.set_humidity_notification(True)
    t.environment.set_color_notification(True)

    try:
        logging.info('entering first try block')
        battery = t.battery.read()
        logging.debug(f"Battery: {battery}")
        while True:
            logging.info('entering while loop')
            t.waitForNotifications(timeout=2)
            if delegate.hasReceivedAll():
                logging.info('all packets received')
                parsed = delegate.received_values.copy()
                parsed["battery"] = battery
                illumination = "light"
                color = parsed["color"]
                if color["red"] == 0 and color["green"] == 0 and color["blue"] == 0:
                    illumination = "dark"

                parsed["illumination"] = illumination

                return parsed

    except Exception as e:
        logging.info('An exception was caught')
        logging.info(e)

    finally:
        logging.info('disconnecting')
        t.disconnect()


