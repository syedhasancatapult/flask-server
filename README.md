# DAS edge code base

The DAS edge is a solution that can create any space into a smart asset tracking space using BLE beacon technology.

This repository and this file will take you throughout the steps you need to install code into a smart gateway. 

# Getting Started

## Equipment

imx-8 gateway with BLE and GPS(SIM7600G or equivalent for your geographical location)

## Prerequisites

- Log into the das machine as the root user
  - `sudo useradd das`
  - `sudo passwd das`
- Generate ssh key to this repository
  - `ssh-keygen -f ~/.ssh/bitbucket`
  - ```eval `ssh-agent -s` ```
  - `ssh-add ~/.ssh/bitbucket`
  - ```echo 'eval `ssh-agent -s`' >> ~/.bashrc ``` - adds ssh-agent initialisation to bashrc
  - ```echo 'ssh-add ~/.ssh/bitbucket' >> ~/.bashrc ``` - adds ssh key to agent
  - `cat ~/.ssh/bitbucket.pub`
  - Copy the output from the last command
  - Go to flask-sever -> repository settings -> Access keys ->Add key -> paste key here
- Clone repository
  - `git clone git@bitbucket.org:syedhasancatapult/flask-server.git`
- Install DAS
  - `cd flask-server`
  - `./install.sh`
### Usage

#### Container Parameters

Build docker image

```shell
docker build -t flask-server .
```

Run docker image

```shell
docker run -p="80:5000" flask-server
```