#!/usr/bin/env python3
from bluepy import btle
import json
import requests
import time
import logging

door_payload_prefix = bytes.fromhex("3906a40164")

def get_door_state_value(payload_bytes):
    door_state_idx = len(door_payload_prefix)

    if len(payload_bytes) == 17 and payload_bytes.startswith(door_payload_prefix):
        state_field = payload_bytes[door_state_idx]
        return state_field

    return None

def parseDoor(scanData):
    manuf_data = scanData[0xff]
    res = get_door_state_value(manuf_data)
    if res is not None and (res == 0 or res == 1):
        return {"door_state":"open" if res else "closed"}
    return None

class DataDispatcher:
    def __init__(self, default_url):
        self.default_url = default_url
        self.mapping = {}

    def addDataHandler(self, addr_list, url, parser):
        self.mapping.update({addr.upper(): (url, parser) for addr in addr_list})

    def getUrl(self, addr):
        addr = addr.upper()
        if addr in self.mapping:
            return self.mapping[addr][0]
        else:
            return self.default_url

    def parseData(self, addr, scanData):
        addr = addr.upper()
        if addr in self.mapping:
            parser = self.mapping[addr][1] 
            if parser is not None:
                return parser(scanData)
            else:
                return None
        else:
            return None



class Scanner:
    class DiscoveryHandler(btle.DefaultDelegate):
        def __init__(self, dispatcher, http_timeout, *p, **v):
            super().__init__(*p, **v)
            self.dispatcher = dispatcher
            self.http_timeout = http_timeout

        def handleDiscovery(self, dev, isNewDev, isNewData):
            if isNewDev or isNewData:
                connectable = 'connectable' if dev.connectable else "not connectable"
                logging.debug(f'    Device: {dev.addr} ({dev.addrType}), {dev.rssi} dBm ({connectable}) count {dev.updateCount}')
                for (sdid, val) in dev.scanData.items():
                    logging.debug(f"\t{hex(sdid)}: {val}")
                if not dev.scanData:
                    logging.debug('\t(no data)')

                url = self.dispatcher.getUrl(dev.addr)
                pkt_payload = {hex(k):v.hex() for k,v in dev.scanData.items()}
                pkt_info = {
                        "addr": dev.addr, 
                        "addr_type": dev.addrType, 
                        "rssi": dev.rssi, 
                        "connectable": dev.connectable,
                        "timestamp": time.strftime("%Y-%m-%d %H:%M:%S"),
                        "payload": pkt_payload}

                parsed_data = self.dispatcher.parseData(dev.addr, dev.scanData)
                if parsed_data is not None:
                    pkt_info["parsed_data"] = parsed_data

                j = json.dumps(pkt_info)
                logging.debug("Sending JSON data: " + j)
                
                try:
                    r = requests.post(
                            url,
                            json=pkt_info,
                            timeout=self.http_timeout)
                except Exception as e:
                    logging.error("Failed to connect to the server")
                    logging.error(e)

    def __init__(self, dispatcher, hci=0, http_timeout=1):
        self.hci = hci
        self.scanner = btle.Scanner(self.hci).withDelegate(self.DiscoveryHandler(dispatcher, http_timeout))

    def scan(self, scan_duration=4):
        logging.debug("Scanning for devices...")
        devices = self.scanner.scan(scan_duration)
        logging.debug("Done scanning")
        return devices

