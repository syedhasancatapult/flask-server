FROM continuumio/miniconda3

WORKDIR /app

COPY environment.yml .
RUN conda env create -f environment.yml

SHELL ["conda", "run", "-n", "flask_server", "/bin/bash", "-c"]
COPY run.py .
COPY templates templates
COPY static static
COPY DeviceClass DeviceClass
COPY Database Database
CMD conda init zsh
ENTRYPOINT ["conda", "run", "--no-capture-output", "-n", "flask_server", "python", "run.py"]
