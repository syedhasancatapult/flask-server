import requests
from requests.exceptions import Timeout, RequestException, ConnectionError
import os, sys
from os.path import join, dirname
from dotenv import load_dotenv
import psycopg2
import json
import boto3
from botocore.exceptions import ClientError
from botocore.client import Config
from datetime import datetime
from starlink import StarlinkPowerCycle as SPC

dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)

sync_requested_satellite = False
# Check if container is online
# Try to connect to db if we can't make a connection, container is offline

def satellite_comms(power:bool):
    spc = SPC()
    if power:
        global sync_requested_satellite
        sync_requested_satellite = True
        spc.control_relay_1_on()
        if not spc.online:
            sys.exit()
        else:
            print("Satellite connection established")
    else:
        spc.control_relay_1_off()
        print("Satellite connection powered down")

url = os.environ.get("CLOUD_API_ENDPOINT") + "sync/"
try:
    r = requests.get(url, timeout=30)
except (Timeout, ConnectionError, RequestException) as e:
    print("Requests exception raised")
    print(e)
    satellite_comms(True)


def http_post(url_location, data):
    return requests.post(url_location, json=data)


def get(sql):
    result = 0
    try:
        conn = psycopg2.connect(
            host="localhost",
            port="5432",
            dbname="das",
            user="postgres",
            password="password"
        )

        cur = conn.cursor()
        sql = sql + ';'
        cur.execute(sql)
        result = cur.fetchall()
    finally:
        try:
            conn.close()
        except:
            pass
    return result


def s3_push(data):
    
    try:
        config = Config(connect_timeout=30, retries={'max_attempts': 2})
        s3 = boto3.client(
            service_name='s3',
            region_name='us-east-2',
            aws_access_key_id='AKIARJNGD2GXPL5BDJWP',
            aws_secret_access_key='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',
            config=config
        )
    except ClientError as s3Error:
        print(s3Error)
        return s3Error
    bucket = 'das-sync-dump'
    file_name = 'ID_{}_'.format(os.environ.get("CONTAINER_ID"))+datetime.now().strftime("%Y%m%d%H%M%S")+'.json'
    print(file_name)
    upload_byte_stream = bytes(json.dumps(data).encode('UTF-8'))
    return s3.put_object(Bucket=bucket, Key=file_name, Body=upload_byte_stream)


def push_data(data):
    id_array = [x['id'] for x in data]
    s3r = s3_push(data)
    print(type(s3r))
    status_code = s3r['ResponseMetadata']['HTTPStatusCode']
    if status_code == 200:
        print('inserted')
        
        # clear local history
        id_array = [value for value in id_array if value != -1]
        if len(id_array)>0:
            delete_history_query = "DELETE FROM history WHERE id IN (" + str(id_array).replace('[', '').replace(']', '') + ")"
            psql_put(delete_history_query)

    if status_code == 403:
        print('error 403')
    if status_code == 404:
        print('error 404')


def psql_put(sql):
    print(sql)
    result = 0
    try:
        conn = psycopg2.connect(
            host="localhost",
            port="5432",
            dbname="das",
            user="postgres",
            password="password"
        )

        cur = conn.cursor()
        cur.execute(sql + ';')
        conn.commit()
    except Exception as e:
        print(e)
        errors = True
    finally:
        try:
            cur.close()
            conn.close()
        except:
            pass

conn = psycopg2.connect(
    host="localhost",
    port="5432",
    dbname="das",
    user="postgres",
    password="password"
)
cur = conn.cursor()
sql_string = """SELECT id, tabname, operation, new_val, old_val FROM history
WHERE NOT (
	operation = 'UPDATE' 
	AND tabname = 'item_container_history'
	AND new_val ->> 'checked_in_out' = 'true'
)
AND NOT (operation = 'DELETE')
ORDER BY tstamp ASC
"""
cur.execute(sql_string)
results = cur.fetchall()
found_first_updates_item_container_history = False
found_all_updates_item_container_history = False
update_row_id = 0
push_arr = []
if len(results) > 0:

    print("""################# pushing {} rows ##################""".format(len(results)))
    for result in results:

        id, table, operation, new_val, old_val = result
        # if operation == "DELETE":
        #     continue
        # if operation == "UPDATE" and table == "item_container_history":
        #     continue
        update = {
            "id": id,
            "table": table,
            "operation": operation,
            "new_val": new_val,
            "old_val": old_val
        }
        push_arr.append(update)

# #####################################
conn = psycopg2.connect(
    host="localhost",
    port="5432",
    dbname="das",
    user="postgres",
    password="password"
)
cur = conn.cursor()
sql_string = """SELECT * FROM item_container_history
WHERE checked_in_out = true
"""
cur.execute(sql_string)
results = cur.fetchall()
for result in results:
    item_history_id, checked_in_out, check_in_ts, item_id, container_id, last_seen_ts = result
    new_val = {
        "item_history_id": item_history_id,
        "checked_in_out": checked_in_out,
        "check_in_ts": check_in_ts.strftime("%m/%d/%Y, %H:%M:%S"),
        "item_id": item_id,
        "container_id": container_id,
        "last_seen_ts": last_seen_ts.strftime("%m/%d/%Y, %H:%M:%S")
    }
    update = {
        "id": -1,
        "table": "item_container_history",
        "operation": "UPDATE",
        "new_val": new_val,
        "old_val": None
    }
    push_arr.append(update)

# ######################################
push_data(push_arr)
    # print(sys.getsizeof(push_arr))
    # r = requests.post(url, json=push_arr)
    # print(r.status_code)
    # print(r.text)
    # if r.status_code == 200:
    #     print('inserted')
    #     # clear local history
    #     sql = "delete from history"
    #     psql_put(sql)
    #
    # if r.status_code == 403:
    #     print('error 403')
    # if r.status_code == 404:
    #     print('error 404')





# Check if there are any changes in the cloud db for this container
# get my last history value

last_history_sync_id_sql = """SELECT * FROM container_state"""
if len(get(last_history_sync_id_sql))>0:
    cs_id, c_id, lhs_id, s_ut = get(last_history_sync_id_sql)[0]
else:
    lhs_id = 0

r_pull = requests.get(url + "pull?last_history_sync_id=" + str(lhs_id))
print(r_pull.json())
for body in r_pull.json():
    print(body)
    id = body['id']
    table_name = body['table']
    operation = body['operation']
    new_val = body['new_val']
    old_val = body['old_val']

    # What table are we dealing with
    try:

        if table_name == "alarm_type":
            print("updating alarm_type table")
            if operation == "INSERT":
                print('inserting into table')
                sql = """ INSERT INTO alarm_type (alarm_type_id, type)
                VALUES ({}, '{}')
                """.format(
                new_val['alarm_type_id'],
                new_val['type']
                )

                psql_put(sql)
            elif operation == "UPDATE":
                sql = """ UPDATE alarm_type
                SET type = '{}',
                WHERE alarm_type_id = {}
                """.format(
                new_val['type'],
                new_val['alarm_type_id']
                )
                psql_put(sql)
        elif table_name == "alarms":

            if operation == "UPDATE":
                if old_val['container_id'] == int(os.environ.get("CONTAINER_ID")):
                    sql = """ UPDATE alarms
                    SET alarm_active_notactive = {}
                    WHERE alarm_id = {}
                    """.format(
                    new_val['alarm_active_notactive'],
                    new_val['alarm_id']
                    )
                    psql_put(sql)

        elif table_name == "container_condition_thresholds":
            print("updating container_condition_thresholds table")
            if operation == "INSERT":
                print('inserting into table')
                if new_val['container_id'] == int(os.environ.get("CONTAINER_ID")):

                    sql = """ INSERT INTO container_condition_thresholds (temp_min, temp_max, humid_min, humid_max, shock_th, light_th, container_id, battery_th)
                    VALUES ({}, {}, {}, {}, {}, {}, {}, {})
                    """.format(new_val['temp_min'], new_val['temp_max'], new_val['humid_min'], new_val['humid_max'], new_val['shock_th'], new_val['light_th'], new_val['container_id'], new_val['battery_th'])

                    psql_put(sql)
            elif operation == "UPDATE":
                print('UPDATING TABLE')
                if new_val['container_id'] == int(os.environ.get("CONTAINER_ID")):
                    sql = """ UPDATE container_condition_thresholds
                    SET temp_min = {},
                    temp_max = {},
                    humid_min = {},
                    humid_max = {},
                    shock_th = {},
                    light_th = {},
                    battery_th = {}
                    WHERE container_id = {}
                    """.format(new_val['temp_min'], new_val['temp_max'], new_val['humid_min'], new_val['humid_max'], new_val['shock_th'], new_val['light_th'], new_val['battery_th'], new_val['container_id'])
                    print("############## sql script #############")
                    print(sql)
                    psql_put(sql)

        elif table_name == "container_stock_thresholds":
            if new_val['container_id'] == int(os.environ.get("CONTAINER_ID")):
                print("updating container_stock_thresholds table")
                item_type_count = """select count(item_type_id) 
                from container_stock_thresholds 
                where item_type_id = {}
                and container_id = {}
                """.format(new_val['item_type_id'], new_val['container_id'])

                count = get(item_type_count)[0]
                count = count[0]
                print(count)

                if operation == "INSERT" and count == 0:
                    print('inserting into table')
                    sql = """ INSERT INTO container_stock_thresholds (amber_th, red_th, container_id, item_type_id)
                    VALUES ({}, {}, {}, {})
                    """.format(new_val['amber_th'], new_val['red_th'], new_val['container_id'], new_val['item_type_id'])

                    psql_put(sql)
                elif operation == "UPDATE":
                    sql = """ UPDATE container_stock_thresholds
                    SET amber_th = {},
                    red_th = {}
                    WHERE container_id = {} and item_type_id = {}
                    """.format(new_val['amber_th'], new_val['red_th'], new_val['container_id'], new_val['item_type_id'])
                    psql_put(sql)
                elif operation == "INSERT" and count == 1:
                    sql = """ UPDATE container_stock_thresholds
                                    SET amber_th = {},
                                    red_th = {}
                                    WHERE container_id = {} and item_type_id = {}
                                    """.format(new_val['amber_th'], new_val['red_th'], new_val['container_id'],
                                               new_val['item_type_id'])
                    psql_put(sql)

        elif table_name == "containers":
            print("updating containers table")
            print(new_val['container_id'])
            print(os.environ.get("CONTAINER_ID"))
            if new_val['container_id'] == int(os.environ.get("CONTAINER_ID")):
                print(new_val['container_id'])
                if operation == "INSERT":
                    print('inserting into table')
                    sql = """ INSERT INTO containers (container_id, description, size, capacity, service_start_ts, service_end_ts)
                    VALUES ({}, '{}', {}, {}, {}, {})
                    """.format(new_val['container_id'],
                               new_val['description'],
                               new_val['size'],
                               new_val['capacity'],
                               'null' if new_val['service_start_ts'] is None or new_val['service_start_ts'] == 'None' else "'" + new_val['service_start_ts'] + "'",
                               'null' if new_val['service_end_ts'] is None or new_val['service_end_ts'] == 'None' else "'" + new_val['service_end_ts'] + "'")
                    psql_put(sql)

                if operation == "UPDATE":
                    sql = """ UPDATE containers
                    SET description = '{}',
                    size = {},
                    capacity = {},
                    service_start_ts = {},
                    service_end_ts = {}
                    WHERE container_id = {}
                    """.format(new_val['description'],
                               new_val['size'],
                               new_val['capacity'],
                               'null' if new_val['service_start_ts'] is None or new_val['service_start_ts'] == 'None' else "'" + new_val['service_start_ts'] + "'",
                               'null' if new_val['service_end_ts'] is None or new_val['service_end_ts'] == 'None' else "'" + new_val['service_end_ts'] + "'",
                               new_val['container_id'])
                    psql_put(sql)

        elif table_name == "device_item":
            print("updating device_item table")
            if operation == "INSERT":
                print('inserting into table')
                sql = """ INSERT INTO device_item (device_item_id, device_id, item_id, timestamp_add, timestamp_remove)
                VALUES ({}, {}, {}, '{}', {})
                """.format(
                    new_val['device_item_id'],
                    new_val['device_id'],
                    new_val['item_id'],
                    new_val['timestamp_add'],
                    'null'
                    )

                psql_put(sql)
            elif operation == "UPDATE":
                sql = """ UPDATE device_item
                SET device_id = {},
                item_id = {},
                timestamp_add = '{}',
                timestamp_remove = {}
                WHERE device_item_id = {}
                """.format(
                    new_val['device_id'],
                    new_val['item_id'],
                    new_val['timestamp_add'],
                    'null',
                    new_val['device_item_id']
                    )
                psql_put(sql)
            elif operation == "DELETE":
                sql = """ DELETE FROM device_item
                WHERE device_id = {}
                """.format(
                    old_val['device_id']
                )
                psql_put(sql)

        elif table_name == "device_type":
            print("updating device_type table")
            if operation == "INSERT":
                print('inserting into table')
                sql = """ INSERT INTO device_type (device_type_id, description, manufacturer, sensors, payload_decoder)
                VALUES ('{}', '{}', '{}', '{}', '{}')
                """.format(
                    new_val['device_type_id'],
                    new_val['description'],
                    new_val['manufacturer'],
                    new_val['sensors'],
                    new_val['payload_decoder'] if not None or not 'None' else 'null'
                    )

                psql_put(sql)
            elif operation == "UPDATE":
                sql = """ UPDATE device_type
                SET description = '{}',
                manufacturer = '{}',
                sensors = '{}',
                payload_decoder = '{}'
                WHERE device_type_id = {}
                """.format(
                    new_val['description'],
                    new_val['manufacturer'],
                    new_val['sensors'],
                    new_val['payload_decoder'] if not None or not 'None' else 'null',
                    new_val['device_type_id']
                    )
                psql_put(sql)
        elif table_name == "devices":
            print("updating devices table")
            if operation == "INSERT":
                print('inserting into table')
                sql = """ INSERT INTO devices (device_id, mac_address, device_type_id)
                VALUES ({}, '{}', {})
                """.format(
                    new_val['device_id'],
                    new_val['mac_address'],
                    new_val['device_type_id']
                    )

                psql_put(sql)
            elif operation == "UPDATE":
                sql = """ UPDATE devices
                SET mac_address = '{}',
                device_type_id = {}
                WHERE device_id = {}
                """.format(
                    new_val['mac_address'],
                    new_val['device_type_id'],
                    new_val['device_id'],
                    )
                psql_put(sql)
        elif table_name == "item_delivery":
            print("updating item_delivery table")

            if operation == "DELETE":
                if old_val['container_id'] == int(os.environ.get("CONTAINER_ID")):
                    sql = """ DELETE FROM item_delivery WHERE item_delivery_id = {}
                    """.format(
                        old_val['item_delivery_id']
                        )

                    psql_put(sql)
            if operation == "INSERT":
                print('inserting into table')
                sql = """ INSERT INTO item_delivery (item_delivery_id, item_id, leadtime, timestamp_request, eta, container_id, quantity)
                VALUES ({}, {}, {}, '{}', '{}', {}, {})
                """.format(
                    new_val['item_delivery_id'],
                    new_val['item_id'],
                    new_val['leadtime'],
                    new_val['timestamp_request'],
                    new_val['eta'],
                    new_val['container_id'],
                    new_val['quantity']
                    )

                psql_put(sql)
            elif operation == "UPDATE":
                sql = """ UPDATE item_delivery
                SET item_id = {},
                leadtime = {},
                timestamp_request = '{}',
                eta = '{}',
                container_id = {},
                quantity = {}
                WHERE item_delivery_id = {}
                """.format(
                    new_val['item_id'],
                    new_val['leadtime'],
                    new_val['timestamp_request'],
                    new_val['eta'],
                    new_val['container_id'],
                    new_val['quantity'],
                    new_val['item_delivery_id']
                    )
                psql_put(sql)
        elif table_name == "item_type":
            print("updating item_type table")

            if operation == "INSERT":
                print('inserting into table')
                sql = """ INSERT INTO item_type (type_id, type, incompatibility, temp_min, temp_max, humid_min, humid_max, shock, description)
                VALUES ({}, '{}', '{}', {}, {}, {}, {}, {}, '{}')
                """.format(
                    new_val['type_id'],
                    new_val['type'],
                    new_val['incompatibility'],
                    new_val['temp_min'],
                    new_val['temp_max'],
                    new_val['humid_min'],
                    new_val['humid_max'],
                    new_val['shock'],
                    new_val['description']
                    )

                psql_put(sql)
            elif operation == "UPDATE":
                sql = """ UPDATE item_type
                SET type = '{}',
                incompatibility = '{}',
                temp_min = {},
                temp_max = {},
                humid_min = {},
                humid_max = {},
                shock = {},
                description = '{}'
                WHERE type_id = {}
                """.format(
                    new_val['type'],
                    new_val['incompatibility'],
                    new_val['temp_min'],
                    new_val['temp_max'],
                    new_val['humid_min'],
                    new_val['humid_max'],
                    new_val['shock'],
                    new_val['description'],
                    new_val['type_id']
                    )
                psql_put(sql)
        elif table_name == "items":
            print("updating items table")
            if operation == "INSERT":
                print('inserting into table')
                sql = """ INSERT INTO items (item_id, expiry_date, alarm, type_id, redacted)
                VALUES ({}, '{}', {}, {}, {})
                """.format(
                    new_val['item_id'],
                    new_val['expiry_date'],
                    new_val['alarm'],
                    new_val['type_id'],
                    new_val['redacted']
                    )

                psql_put(sql)
            elif operation == "UPDATE":
                sql = """ UPDATE items
                SET expiry_date = '{}',
                alarm = {},
                type_id = {},
                redacted = {}
                WHERE item_id = {}
                """.format(
                    new_val['expiry_date'],
                    new_val['alarm'],
                    new_val['type_id'],
                    new_val['redacted'],
                    new_val['item_id']
                    )
                psql_put(sql)

        elif table_name == "user_role":
            print("updating user_role table")
            if operation == "INSERT":
                print('inserting into table')
                sql = """ INSERT INTO user_role (role_id, type)
                VALUES ({}, '{}')
                """.format(
                    new_val['role_id'],
                    new_val['type']
                    )

                psql_put(sql)
            elif operation == "UPDATE":
                sql = """ UPDATE user_role
                SET type = '{}',
                WHERE role_id = {}
                """.format(
                    new_val['type'],
                    new_val['role_id']
                    )
                psql_put(sql)

        elif table_name == "users":
            print("updating users table")
            if operation == "INSERT":
                print('inserting into table')
                sql = """ INSERT INTO users (user_id, name, username, password, accesscard, role_id, active)
                VALUES ({}, '{}', '{}', '{}', '{}', {}, {})
                """.format(
                    new_val['user_id'],
                    new_val['name'],
                    new_val['username'],
                    new_val['password'],
                    new_val['accesscard'],
                    new_val['role_id'],
                    new_val['active']
                    )

                psql_put(sql)
            elif operation == "UPDATE":
                sql = """ UPDATE users
                SET name = '{}',
                username = '{}',
                password = '{}',
                accesscard = '{}',
                role_id = {},
                active = {}
                WHERE user_id = {}
                """.format(
                    new_val['name'],
                    new_val['username'],
                    new_val['password'],
                    new_val['accesscard'],
                    new_val['role_id'],
                    new_val['active'],
                    new_val['user_id']
                    )
                psql_put(sql)
    except Exception as e:
        print(str(e))
    # Reset timer

# update last_history_sync_id into container_state
if len(r_pull.json()) > 0:

    last_history_sync_id = max(r_pull.json(), key=lambda x: x['id'])['id']

    container_state = "SELECT * FROM container_state"
    if len(get(container_state)) == 0:
        sql = """ INSERT INTO container_state (container_state_id, container_id, last_history_sync_id, system_uptime)
                        VALUES ({}, {}, {}, {})
                        """.format(
                                1,
                                int(os.environ.get("CONTAINER_ID")),
                                last_history_sync_id,
                                'null'
        )
        psql_put(sql)
    else:
        sql_update_container_state = """UPDATE container_state
        SET last_history_sync_id = {}
        WHERE container_state_id = 1
        """.format(last_history_sync_id)
        psql_put(sql_update_container_state)


sql_delete_history_junk = """DELETE FROM history 
WHERE (operation = 'UPDATE' AND tabname = 'item_container_history')
OR (operation = 'DELETE')
"""
psql_put(sql_delete_history_junk)

# power down satellite
# check if service is running before turning off
check_service_sql = sql = """SELECT service_start_ts, service_end_ts
FROM containers
WHERE container_id = {}
""".format(int(os.environ.get("CONTAINER_ID")))
startTs, endTs = get(sql)[0]
currentTs = datetime.now()
print(startTs)
print(endTs)
print(currentTs)

if (startTs or endTs is not None) and startTs < currentTs < endTs:
    print("in service")
    sys.exit()
else:
    if sync_requested_satellite:
        satellite_comms(False)
